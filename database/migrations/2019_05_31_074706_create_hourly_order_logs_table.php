<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateHourlyOrderLogsTable.
 */
class CreateHourlyOrderLogsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hourly_order_logs', function(Blueprint $table) {
            $table->increments('id');
            $table->date('day');
            $table->integer('hour');
            $table->integer('order_count');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('hourly_order_logs');
	}
}
