<?php

declare(strict_types=1);

namespace Database\Seeds;

use App\Common\Interfaces\Runnable;
use Illuminate\Database\Seeder as AbstractSeeder;
use Illuminate\Support\Facades\DB;

/**
 * Abstract Seeder
 * 
 * This class is a seeder parent handles things related to report seeding status
 */

abstract class Seeder extends AbstractSeeder implements Runnable
{
    /**
     * __construct
     * @return void
     */
    public function __construct()
    {
        $this->validateTable();
    }

    /**
     * validateTable
     */
    public function validateTable()
    {
        $seederName = $this->getSeederName();
        $tableName = $this->getTableName();

        if (empty($tableName)) {
            $this->printError(' # Table name has not been set. Please define "getTableName" for the seed '.$seederName.' and run again!');
            exit(1);
        }
        
        return true;
    }
    
    /**
     * Get seeder name.
     *
     * @return string
     */
    abstract public function getSeederName(): string;

    /**
     * Print information to console log.
     *
     * @param $message
     */
    public function print($message)
    {
        if (isset($this->command)) {
            $this->command->info($message);
            return;
        }
        echo $message;
    }

    /**
     * Print information to console log.
     *
     * @param $message
     */
    public function printWarn($message)
    {
        if (isset($this->command)) {
            $this->command->warn($message);
            return;
        }
        echo $message;
    }

    /**
     * Print information to console log.
     *
     * @param $message
     */
    public function printError($message)
    {
        if (isset($this->command)) {
            $this->command->error($message);
            return;
        }
        echo $message;
    }

    /**
     * Show success message to console.
     */
    public function showSuccessMessage()
    {
        $tableName = $this->getTableName();
        $this->print(" # Verify seeding: Table [$tableName] has been seeded successfully!");
    }

    /**
     * Show fail message to console.
     */
    public function showFailMessage()
    {
        $tableName = $this->getTableName();
        $this->printError(" # Verify seeding: Table [$tableName] has been seeded fail!");
        exit(1);
    }

    /**
     * Get number of rows in current table
     * This method is abstract because we can not indentify
     * which table should be retrieved.
     *
     * @return int
     */
    abstract protected function getNumberOfRows(): int;

    /**
     * Verify by number of records
     * 
     * We want to make sure all data (or in sql files) are imported
     * with an estimation or exactly number
     *
     * @param $numberOfRecords
     */
    protected function assertNumberOfRowsEquals($numberOfRecords)
    {
        $seederName = $this->getSeederName();
        $tableName = $this->getTableName();

        $actualNumberOfRecords = $this->getNumberOfRows();
        if ($actualNumberOfRecords === $numberOfRecords) {
            $this->print(" + Imported $actualNumberOfRecords record(s) for table [$tableName]");
            $this->showSuccessMessage();
        } else {
            $this->printWarn(" - Actual number of records in seeder [$tableName] is $actualNumberOfRecords");
            $this->printWarn(" - Expected number of records in seeder [$tableName] is  $numberOfRecords");
            $traces = \debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 3);
            if (!empty($traces) && \count($traces) > 0) {
                $trace = $traces[0];
                $this->printError(' + Assert fail: ' . $trace['file'] . ':' . $trace['line']);
            }
            $this->showFailMessage();
        }
    }

    /**
     * Verify by record
     * 
     * We want to make sure the data (or in sql files) are imported
     * with an estimation or exactly record
     *
     * @param $record
     */
    protected function assertRecord($record)
    {
        $seederName = $this->getSeederName();
        $tableName = $this->getTableName();

        $recordText = json_encode($record);

        $existedRecord = DB::table($tableName)->where($record)->get()->toArray();
        if (!empty($existedRecord)) {
            $this->print(" + Imported right record [$recordText] for table [$tableName]");
            $this->showSuccessMessage();
        } else {
            $this->printWarn(" - No record data in table [$tableName] found");
            $this->printWarn(" - Expected record in table [$tableName] is  $recordText");
            $traces = \debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 3);
            if (!empty($traces) && \count($traces) > 0) {
                $trace = $traces[0];
                $this->printError(' + Assert fail: ' . $trace['file'] . ':' . $trace['line']);
            }
            $this->showFailMessage();
        }
    }
}
