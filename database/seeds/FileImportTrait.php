<?php

declare(strict_types=1);

namespace Database\Seeds;

use App\Common\Helpers\CharsetHelper;
use App\Common\Helpers\StringHelper;
use Illuminate\Support\Facades\DB;

/**
 * File Import Trait
 * 
 * This trait supports for importing data to database
 */
trait FileImportTrait
{
    protected function getPath($path)
    {
        return base_path() . DIRECTORY_SEPARATOR . 'database' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . $path;
    }

    /**
     * Print error.
     *
     * @param $message
     */
    public function printError($message)
    {
        if (isset($this->command)) {
            $this->command->error($message);

            return;
        }
        echo $message;
    }

    /**
     * Import data from a file name.
     *
     * @param string $fileName
     * @param string $charset
     *
     * @return string || boolean
     */
    protected function importFile($fileName, $charset = CharsetHelper::CHARSET_SJIS)
    {
        $seedFilePath = base_path() . DIRECTORY_SEPARATOR . 'database' . DIRECTORY_SEPARATOR . 'files';
        $filePath = $seedFilePath . DIRECTORY_SEPARATOR . $fileName;
        if (!\file_exists($filePath) || !\is_readable($filePath)) {
            $this->printError("Can not read file : $filePath");

            return false;
        }

        // Detect file format
        $filePathArr = explode(".", $filePath);
        $fileFormat = end($filePathArr);

        if ($fileFormat === 'sql') {
            return $this->importSQL($filePath);
        }

        if ($fileFormat === 'csv') {
            return $this->importCSV($filePath, $charset);
        }

        return "Unsupport file format: $fileFormat";
    }

    /**
     * Import SQL File.
     *
     * @param $sqlFile
     *
     * @return bool
     */
    private function importSQL($sqlFile)
    {
        DB::unprepared(\file_get_contents($sqlFile));
    }

    /**
     * Import CSV.
     *
     * @param $csvFile
     * @param $charset
     *
     * @return bool
     */
    private function importCSV($csvFile, $charset)
    {
        $handle = \fopen($csvFile, 'r');
        if ($handle === false) {
            $this->printError("Can not open CSV file $csvFile");
            exit(1);
        }

        $columns = \fgetcsv($handle);
        $columnString = '';
        foreach ($columns as $column) {
            $columnString .= $column . ',';
        }
        $columnString = \mb_substr($columnString, 0, -1);
        $table = $this->getTableName();
        $dataDelimiter = StringHelper::getDelimiter();

        $rawQuery = "LOAD DATA LOCAL INFILE '%s' INTO TABLE $table
            CHARACTER SET $charset
            FIELDS TERMINATED BY '\",\"'
            LINES STARTING BY '\"' 
            TERMINATED BY '\"$dataDelimiter'
            IGNORE 1 LINES ($columnString)";

        $query = \sprintf($rawQuery, \addslashes($csvFile));
        $result = $this->runQueryinRawMode($query);

        if ($this->getNumberOfRows() === 0) {
            $rawQuery = "LOAD DATA LOCAL INFILE '%s' INTO TABLE $table
            CHARACTER SET $charset
            FIELDS TERMINATED BY ','
            LINES TERMINATED BY '$dataDelimiter'
            IGNORE 1 LINES ($columnString)";
            $query = \sprintf($rawQuery, \addslashes($csvFile));
            $result = $this->runQueryinRawMode($query);
        }

        if ($result === false) {
            $this->printError('Can not execute query !');
            $this->printError($query);
            exit(1);
        }
    }
}
