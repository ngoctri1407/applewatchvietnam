<?php

declare(strict_types=1);

namespace Database\Seeds;

/**
 * Abstract SQL File Seeder
 * 
 * This abstract class allows us to seed data from SQL file
 * 
 * @category  \Database\Seeder
 */
abstract class SQLFileSeeder extends SQLSeeder
{
    /**
     * This trait support for importing a file
     * we have two kind of format are CSV and SQL.
     */
    use FileImportTrait;
}
