<?php

declare(strict_types=1);

namespace Database\Seeds;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

/**
 * Abstract SQL Seeder
 * 
 * This abstract class allows us to seed data from SQL
 * 
 * @category  \Database\Seeder
 */
abstract class SQLSeeder extends Seeder
{
    /**
     * __construct
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Limit number of records
     */
    const LIMIT_RECORDS = 100;
    
    /**
     * Show full seeder name or not
     */
    const SHOW_FULL_SEEDER_NAME = 1;

    /**
     * Limit number of records
     *
     * @var int
     */
    private $limitRecords = self::LIMIT_RECORDS;

    /**
     * Check to show full seeder name
     *
     * @var int
     */
    private $showFullSeederName = self::SHOW_FULL_SEEDER_NAME;

    /**
     * Get seeder name
     */
    public function getSeederName(): string
    {
        $reflectionClass = new \ReflectionClass($this);
        if ($this->showFullSeederName) {
            return $reflectionClass->getName();
        }
        return $reflectionClass->getShortName();
    }

    /**
     * Set limit for records
     * Override maximum number of records can be imported
     *
     * @param int $limitRecords
     */
    public function setLimitRecords(int $limitRecords)
    {
        $this->limitRecords = $limitRecords;
    }

    /**
     * Get table name.
     *
     * @return string
     */
    abstract protected function getTableName();

    /**
     * Get the limitation of records
     *
     * @return int
     */
    protected function getLimitRecords()
    {
        return $this->limitRecords;
    }

    /**
     * Truncate all records from a specific table
     */
    protected function truncateTable()
    {
        $tableName = $this->getTableName();
        $seederName = $this->getSeederName();

        $this->print(' - Truncating table ' . $tableName);
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table($this->getTableName())->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        $this->print(' - Truncated table ' . $this->getTableName());
    }

    /**
     * Insert a record.
     *
     * @param $record
     */
    protected function insertRecord($record)
    {
        DB::table($this->getTableName())->insert($record);
    }

    /**
     * Insert multiple records at once time
     * If number of records reach the limitation then insertion will be stopped
     *
     * @param $records
     */
    protected function insertRecords($records)
    {
        $recordsCount = !empty($records) ? \count($records) : 0;
        $this->print(' - Preparing seeding ' . $recordsCount . ' record(s) ...');
        $counter = 0;
        foreach ($records as $record) {
            if ($counter >= $this->getLimitRecords()) {
                $this->printWarn(
                    ' - Insert multiple records: you reached the default limitation is ' .
                    $this->getLimitRecords() .
                    '. Please use $this->setLimitRecords(int) to custom number of records'
                );
                break;
            }
            $this->insertRecord($record);
            ++$counter;
        }
    }

    /**
     * Get number of rows in current table
     * This function is belong to Laravel database connection
     *
     * @return int
     */
    protected function getNumberOfRows(): int
    {
        $table = $this->getTableName();
        return (int) DB::table($table)->count();
    }

    /**
     * Run query in raw mode.
     * 
     * This function is not belong to Laravel database connection
     * 
     * @param query
     * @param string $query
     *
     * @return mixed
     */
    protected function runQueryInRawMode($query)
    {
        $config = Config::get('database')['connections']['mysql'];
        // Create connection
        $conn = \mysqli_connect($config['host'], $config['username'], $config['password']);
        if (!$conn) {
            $this->printError(' + Connection failed: ' . \mysqli_connect_error());
            exit(1);
        }
        \mysqli_select_db($conn, $config['database']);
        $result = \mysqli_query($conn, $query);
        
        return $result;
    }
}
