<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Entities\Customer;
use App\Entities\Setting;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $amount_product = Setting::where('key','amount_product')->orderBy('created_at', 'desc')->first();
        if(!$amount_product){
            $amount_product = 3243;
        }else{
            $amount_product = $amount_product->value;
        }
        return view('home.index')->with('amount_product',$amount_product);
    }
    public function orderSuccess()
    {
        $code = Input::get('code');
        if(!$code || $code ==''){
            return redirect('/');
        }
        if($code != 'SUCCESS'){
            $customer = Customer::where('code',$code)->first();
            if(!$customer){
                return redirect('/');
            }
            if($customer->color == 'silver'){
                $link_img = './images/40-alu-silver-sport-white-nc-s4-1up.png';
            }
            if($customer->color == 'gold'){
                $link_img = './images/44-alu-gold-sport-pink-sand-s4-gallery1.jpeg';
            }
            if($customer->color == 'black'){
                $link_img = './images/44-alu-space-sport-black-s4-gallery1.jpeg';
            }
        }
        else{
            $customer = Customer::first();
            $link_img = './images/40-alu-silver-sport-white-nc-s4-1up.png';
        }

        return view('home.orderSuccess')->with(['code'=>$code,'customer'=>$customer,'link_img'=>$link_img]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function about()
    {
        return view('home.about');
    }

    public function loan()
    {
        return view('home.loan');
    }
}
