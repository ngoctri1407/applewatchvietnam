<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Customer;
use App\Entities\Setting;

class DashboardController extends Controller
{
    public function index()
    {
        $total = Customer::where('status','!=',2)->sum('price');
        $success = Customer::where('status',1)->sum('price');
        $waiting = Customer::where('status',0)->sum('price');
        $cost =Setting::where('key','facebook_ads')->sum('value');
        return view('admin.home.index')
            ->with([
                'success'=>$success ,
                'waiting'=>$waiting,
                'total'=>$total,
                'cost'=>$cost
            ]);
    }
}
