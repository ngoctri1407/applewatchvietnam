<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(\App\Repositories\LoanRepository::class, \App\Repositories\LoanRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\LoanFieldRepository::class, \App\Repositories\LoanFieldRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\LoanFieldRepository::class, \App\Repositories\LoanFieldRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\WidgetRepository::class, \App\Repositories\WidgetRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ImageRepository::class, \App\Repositories\ImageRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CategoryRepository::class, \App\Repositories\CategoryRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\BrandRepository::class, \App\Repositories\BrandRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\GoodsRepository::class, \App\Repositories\GoodsRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\UserRepository::class, \App\Repositories\UserRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CustomerRepository::class, \App\Repositories\CustomerRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\SettingRepository::class, \App\Repositories\SettingRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\HourlyOrderLogRepository::class, \App\Repositories\HourlyOrderLogRepositoryEloquent::class);
        //:end-bindings:
    }
}
