<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        require_once __DIR__ . '/../Http/Helpers/Navigation.php';
        $this->app->register(RepositoryServiceProvider::class);
        $services = require(base_path() . '/config/bind.php');
        foreach ($services as $value) {
            $this->app->singleton($value, $value);
        }
    }
}
