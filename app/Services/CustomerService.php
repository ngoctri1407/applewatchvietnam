<?php

declare(strict_types=1);

/**
 * ---------------------------------------------------------------------------------------
 * Copyright (c) 2019 CashTech Project. All rights reserved.
 * ---------------------------------------------------------------------------------------
 * NOTICE:  All information contained herein is, and remains
 * the property of GKC and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to GKC
 * and its suppliers and may be covered by Vietnamese Law,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from GKC.
 * ---------------------------------------------------------------------------------------
 * Author: Thach Tran <thachtran@giakiemcoder.com>
 * ---------------------------------------------------------------------------------------
 */

namespace App\Services;


use App\Entities\Customer;
use App\Entities\Setting;
use App\Repositories\CustomerRepository;

/**
 * Class CustomerService
 * @package App\Services
 */
class CustomerService
{
    /**
     * @var CustomerRepository
     */
    private $customerRepository;


    /**
     * CustomerService constructor.
     * @param CustomerRepository $customerRepository
     */
    public function __construct(CustomerRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        $customer = $this->customerRepository->create($data);
        $amount_product = Setting::where('key','amount_product')->latest()->first();;
        if($amount_product){
            $amount_product->value = $amount_product->value - mt_rand(1,10);
            $amount_product->save();
        }

        return $customer;
    }

    public function all()
    {
        return $this->customerRepository->all();
    }
    public function delete($id)
    {
        return $this->customerRepository->delete($id);
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     */
    public function update($data, $id)
    {
        $customer = $this->customerRepository->update($data, $id);
        return $customer;
    }
}