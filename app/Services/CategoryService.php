<?php

declare(strict_types=1);

/**
 * ---------------------------------------------------------------------------------------
 * Copyright (c) 2019 CashTech Project. All rights reserved.
 * ---------------------------------------------------------------------------------------
 * NOTICE:  All information contained herein is, and remains
 * the property of GKC and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to GKC
 * and its suppliers and may be covered by Vietnamese Law,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from GKC.
 * ---------------------------------------------------------------------------------------
 * Author: Thach Tran <thachtran@giakiemcoder.com>
 * ---------------------------------------------------------------------------------------
 */

namespace App\Services;


use App\Entities\Category;
use App\Repositories\BrandRepository;
use App\Repositories\CategoryRepository;

/**
 * Class CategoryService
 * @package App\Services
 */
class CategoryService
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var BrandRepository
     */
    private $brandRepository;

    /**
     * CategoryService constructor.
     * @param CategoryRepository $categoryRepository
     * @param BrandRepository $brandRepository
     */
    public function __construct(CategoryRepository $categoryRepository, BrandRepository $brandRepository)
    {
        $this->categoryRepository = $categoryRepository;
        $this->brandRepository = $brandRepository;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        $category = $this->categoryRepository->create($data);

        if (!empty($data['brands']) && is_array($data['brands'])) {
            foreach ($data['brands'] as $brand) {

                $this->brandRepository->create([
                    'title' => $brand['brand_title'],
                    'category_id' => $category->id
                ]);
            }
        }

        return $category;
    }

    public function all()
    {
        return $this->categoryRepository->all();
    }
}