<?php

declare(strict_types=1);

/**
 * ---------------------------------------------------------------------------------------
 * Copyright (c) 2019 CashTech Project. All rights reserved.
 * ---------------------------------------------------------------------------------------
 * NOTICE:  All information contained herein is, and remains
 * the property of GKC and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to GKC
 * and its suppliers and may be covered by Vietnamese Law,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from GKC.
 * ---------------------------------------------------------------------------------------
 * Author: Thach Tran <thachtran@giakiemcoder.com>
 * ---------------------------------------------------------------------------------------
 */

namespace App\Services;
use Intervention\Image\ImageManagerStatic;
use App\Repositories\ImageRepository;

class ImageService
{
    private $imageRepository;

    public function __construct(ImageRepository $imageRepository)
    {
        $this->imageRepository = $imageRepository;
    }

    const SIZES = [
        'thumbnail' => ['width' => 480, 'height' => 480],
    ];
    public function upload($file)
    {
        $filename = sha1(time() . time());

        $image_resize = ImageManagerStatic::make($file->getRealPath());

        // Original
        $original_path = "upload/$filename.{$file->getClientOriginalExtension()}";
        $image_resize->save(public_path($original_path));

        $path = [
            'original' => $original_path
        ];

        foreach (self::SIZES as $key=>$size) {
            $image_resize->resize($size['width'], $size['height']);
            $size_path = "upload/$filename-$key.{$file->getClientOriginalExtension()}";
            $image_resize->save(public_path($size_path));
            $path[$key] = $size_path;
        }

        $image = $this->imageRepository->create([
            'name' => $file->getClientOriginalName(),
            'path' => json_encode($path)
        ]);

        return [
            'id' => $image->id,
            'path' => $path
        ];
    }
}