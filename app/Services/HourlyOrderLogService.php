<?php

declare(strict_types=1);

/**
 * ---------------------------------------------------------------------------------------
 * Copyright (c) 2019 CashTech Project. All rights reserved.
 * ---------------------------------------------------------------------------------------
 * NOTICE:  All information contained herein is, and remains
 * the property of GKC and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to GKC
 * and its suppliers and may be covered by Vietnamese Law,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from GKC.
 * ---------------------------------------------------------------------------------------
 * Author: Thach Tran <thachtran@giakiemcoder.com>
 * ---------------------------------------------------------------------------------------
 */

namespace App\Services;


use App\Entities\HourlyOrderLog;
use App\Repositories\HourlyOrderLogRepository;

/**
 * Class HourlyOrderLogService
 * @package App\Services
 */
class HourlyOrderLogService
{
    /**
     * @var HourlyOrderLogRepository
     */
    private $HourlyOrderLogRepository;


    /**
     * HourlyOrderLogService constructor.
     * @param HourlyOrderLogRepository $HourlyOrderLogRepository
     */
    public function __construct(HourlyOrderLogRepository $HourlyOrderLogRepository)
    {
        $this->HourlyOrderLogRepository = $HourlyOrderLogRepository;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        $HourlyOrderLog = $this->HourlyOrderLogRepository->create($data);


        return $HourlyOrderLog;
    }

    public function all()
    {
        return $this->HourlyOrderLogRepository->all();
    }
    public function delete($id)
    {
        return $this->HourlyOrderLogRepository->delete($id);
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     */
    public function update($data, $id)
    {
        $HourlyOrderLog = $this->HourlyOrderLogRepository->update($data, $id);
        return $HourlyOrderLog;
    }
}