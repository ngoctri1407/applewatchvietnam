<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Hourly_order_logRepository;
use App\Entities\HourlyOrderLog;
use App\Validators\HourlyOrderLogValidator;

/**
 * Class HourlyOrderLogRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class HourlyOrderLogRepositoryEloquent extends BaseRepository implements HourlyOrderLogRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return HourlyOrderLog::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
