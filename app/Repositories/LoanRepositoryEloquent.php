<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\LoanRepository;
use App\Entities\Loan;
use App\Validators\LoanValidator;

/**
 * Class LoanRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class LoanRepositoryEloquent extends BaseRepository implements LoanRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Loan::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
