<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface HourlyOrderLogRepository.
 *
 * @package namespace App\Repositories;
 */
interface HourlyOrderLogRepository extends RepositoryInterface
{
    //
}
