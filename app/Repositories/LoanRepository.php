<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface LoanRepository.
 *
 * @package namespace App\Repositories;
 */
interface LoanRepository extends RepositoryInterface
{
    //
}
