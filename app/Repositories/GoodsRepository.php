<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface GoodsRepository.
 *
 * @package namespace App\Repositories;
 */
interface GoodsRepository extends RepositoryInterface
{
    //
}
