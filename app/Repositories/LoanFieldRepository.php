<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface LoanFieldRepository.
 *
 * @package namespace App\Repositories;
 */
interface LoanFieldRepository extends RepositoryInterface
{
    //
}
