<?php

declare(strict_types=1);

namespace App\Common\Helpers;

/**
 * String Helper
 *
 * @category  App\Common\Helpers
 */
class StringHelper
{
    /**
     * Get data delimiter.
     *
     * @return string
     */
    public static function getDelimiter()
    {
        if (\mb_strtoupper(\mb_substr(PHP_OS, 0, 3)) === 'WIN') {
            return "\r\n";
        }

        return "\n";
    }

    /**
     * Get translated phrases based on specified keys
     * @param array $keys
     * @param null $prefix
     * @return array
     */
    public static function getPhrases($keys = [], $prefix = null)
    {
        $phrases = [];
        foreach ($keys as $key => $value) {
            if (is_array($value)) {
                $phrases[$key] = self::getPhrases($value, $prefix ? $prefix . '.' . $key : $key);
            } else {
                $phrases[$value] = __($prefix ? $prefix . '.' . $value : $value);
            }
        }
        return $phrases;
    }

    public static function removeMetaCharacter($string)
    {
        return isset($string) ? str_replace(["\r", "\n", "\r\n", "\n\r"], " ", $string) : "";
    }
}