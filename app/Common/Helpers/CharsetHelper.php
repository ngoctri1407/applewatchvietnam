<?php

declare(strict_types=1);

namespace App\Common\Helpers;

/**
 * Charset Helper
 * 
 * @category  App\Common\Helpers
 */
class CharsetHelper
{
    const CHARSET_UTF_8 = 'UTF8';
}
