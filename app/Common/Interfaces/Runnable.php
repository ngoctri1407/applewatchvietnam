<?php

declare(strict_types=1);

namespace App\Common\Interfaces;

/**
 * Runnable Interface
 * 
 * @category  App\Common\Interfaces
 */

interface Runnable
{
    /**
     * Runnable method.
     */
    public function run();
}
