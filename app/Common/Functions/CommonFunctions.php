<?php

declare(strict_types=1);

/**
 * ---------------------------------------------------------------------------------------
 * Copyright (c) 2019 CashTech Project. All rights reserved.
 * ---------------------------------------------------------------------------------------
 * NOTICE:  All information contained herein is, and remains
 * the property of GKC and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to GKC
 * and its suppliers and may be covered by Vietnamese Law,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from GKC.
 * ---------------------------------------------------------------------------------------
 * Author: Thach Tran <thachtran@giakiemcoder.com>
 * ---------------------------------------------------------------------------------------
 */

/**
 * Override resource with version
 *
 * @param string $file
 * @return bool|string
 */
function resource($file = '') {
    $versionFile = base_path() . '/version';
    $versionName = file_get_contents($versionFile);
    $versionName = str_replace(array("\r", "\n"), '', $versionName);
    if (empty($file)) {
        return $versionName;
    }
    return url($file) . "?v=$versionName";
}
