<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumnLatlngAddColumnPositionTableLocations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('locations', function (Blueprint $table) {
            //
//            $table->dropIfExists('latitude');
//            $table->dropIfExists('longitude');
            $table->string('position')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('locations', function (Blueprint $table) {
            //
//            $table->float('latitude',16,8);
//            $table->float('longitude',16,8);
            $table->dropIfExists('position');
        });
    }
}
