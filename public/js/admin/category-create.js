$(document).ready(function () {
    Dropzone.options.fileDropzone = {
        maxFiles:1,
        init: function() {
            this.hiddenFileInput.removeAttribute('multiple');
            this.on("maxfilesexceeded", function (file) {
                this.removeAllFiles();
                this.addFile(file);
            });

            this.on("success", function(file, response) {
                $('[name=image]').val(response.id);
            })
        }
    };

    let $brands_table = $('#brands-table').dynamicTable({
        required_fields: ['brand_title']
    });


    $('#category-create-form').on('submit', function (e) {
        e.preventDefault();

        $.ajax({
            url: $(this).prop('action'),
            data: {
                title: $('input[name=title]').val(),
                image: $('input[name=image]').val(),
                brands: $brands_table.getData()
            },
            method: 'post',
            success: function (response) {
                if (response.status && (response.status == true)) {
                    swal(phrases.title.success, response.message, "success");
                } else {
                    swal(phrases.title.error, response.message, "error");
                }
            },
        });
    })
});
//# sourceMappingURL=category-create.js.map
