<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();

Route::group(['middleware' => ['auth']], function() {
    Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
        Route::get('/', 'DashboardController@index')->name('dashboard');
        Route::resource('category', 'CategoryController');
        Route::resource('customer', 'CustomerController');
        Route::resource('settings', 'SettingController');

    });

    Route::resource('file', 'FileController');

    Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
});

Route::get('/', 'HomeController@index')->name('home');
Route::get('/order', 'Admin\CustomerController@store')->name('order');
Route::get('/orderSuccess', 'HomeController@orderSuccess')->name('orderSuccess');
Route::get('/about', 'HomeController@about')->name('about');