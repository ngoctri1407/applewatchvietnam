<?php

declare(strict_types=1);

/**
 * ---------------------------------------------------------------------------------------
 * Copyright (c) 2019 CashTech Project. All rights reserved.
 * ---------------------------------------------------------------------------------------
 * NOTICE:  All information contained herein is, and remains
 * the property of GKC and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to GKC
 * and its suppliers and may be covered by Vietnamese Law,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from GKC.
 * ---------------------------------------------------------------------------------------
 * Author: Thach Tran <thachtran@giakiemcoder.com>
 * ---------------------------------------------------------------------------------------
 */

return [
    'createNew' => 'Tạo mới',
    'list' => 'Danh sách',
    'image' => 'Hình ảnh',
    'save' => 'Lưu',
    'cancel' => 'Hủy',
    'success' => 'Thành công',
    'error' => 'Lỗi!',
    'delete' => 'Xóa',
    'edit' => 'Sửa',
    'actions' => 'Thao tác',
    'home' => 'Trang chủ',
    'logout' => 'Đăng xuất'
];