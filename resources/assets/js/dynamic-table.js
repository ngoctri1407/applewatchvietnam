/*
Dynamic Table
Support for type of tables which can be add or remove row dynamically

@author: vfa.thachtc
@param Object options
    - removal_alert: null or false for non alert popup shows before removing row. Object {text: [Confirm message]} if you want to show a confirm popup
    - removed_callback: this function will be run after removing a row
    - inserted_callback: this function will be run after inserting a row
@return
    Function getData - return data of all rows in table
    Function setData - set an array of data for all rows in table
 */
$.fn.dynamicTable = function (options = {}) {
    const default_options = {
        required_fields: [],
        removal_alert: false,
        removed_callback: (row_data) => {
        },
        inserted_callback: ($row) => {
        },
        row_validate: ($row, row_data) => true,
        require_all_valid: true,
        row_template_count: 1,
        view_only: false,
    };
    options = {...default_options, ...options};
    const $table = $(this);
    const template_definitions = {
        row_template: '.dt-row-template',
        insert_row: '.dt-insert-row',
        remove_row: '.dt-remove-row',
        data_row: 'tbody tr:not(.dt-row-template)'
    };

    $.dtGet = function (selector) {
        return $table.find(selector);
    };

    const $row_template = $.dtGet(template_definitions.row_template);

    let insert_row_fn = ($row = null) => {
        let $new_row = $row_template.clone();

        $new_row.removeClass('dt-row-template');
        $new_row.toggleClass('animated flipInX');
        options.required_fields.forEach(function (name) {
            $new_row.find(`[name=${name}]`).prop('required', 'required');
        });
        if ($row) {
            if (options.row_template_count > 1) {
                for (let i = 1; i < options.row_template_count; i++) {
                    $row = $row.next();
                }
            }
            $new_row.insertAfter($row);

        } else {
            $new_row.appendTo($table.find('tbody'));
        }

        check_lock_remove_fn();
        // Execute inserted callback
        options.inserted_callback($new_row);
        return $new_row;
    };

    let remove_row_fn = ($row) => {
        let do_action = () => {
            let row_data = get_row_data_fn($row);
            let $remove_rows = [$row];
            for (let i = 1; i < options.row_template_count; i++) {
                $remove_rows.push($row.next());
            }

            $remove_rows.forEach($row => {
                $row.toggleClass('animated flipInX').toggleClass('animated bounceOutLeft');
            });


            setTimeout(function () {
                $remove_rows.forEach($row => {
                    $row.remove();
                });

                check_lock_remove_fn();
                // Execute a callback function after removing row
                options.removed_callback(row_data);

            }, 500)
        };
        if (options.removal_alert !== false) {
            if (typeof swal !== "undefined") {
                swal({
                    title: phrases.title.confirm,
                    text: options.removal_alert.text,
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: phrases.title.cancel,
                    confirmButtonText: phrases.title.ok,
                    dangerMode: true,
                }, (value) => {
                    if (value) {
                        do_action();
                    }
                });
            } else {
                if (confirm(options.removal_alert.text)) {
                    do_action();
                }
            }
        } else {
            do_action();
        }
    };

    let check_lock_remove_fn = () => {
        if (count_data_rows_fn() > options.row_template_count) {
            $.dtGet(template_definitions.data_row).find(template_definitions.remove_row).removeAttr('disabled');
        } else {
            $.dtGet(template_definitions.data_row).find(template_definitions.remove_row).attr('disabled', 'disabled');
        }
    };

    let count_data_rows_fn = () => {
        return $.dtGet(template_definitions.data_row).length;
    };

    let init_fn = () => {
        if($table.hasClass('dynamic-table-initialized')) {
            return;
        }

        $table.on('click', template_definitions.insert_row, function () {
            insert_row_fn($(this).closest('tr'));
        });
        $table.on('click', template_definitions.remove_row, function () {
            remove_row_fn($(this).closest('tr'));
        });

        $table.on('change', 'input, select, textarea', function () {
            let $row = $(this).closest('tr');
            if (!$row.closest('tbody').length) {
                return;
            }
            let row_data = get_row_data_fn($row);
            options.row_validate($row, row_data);
        });

        if (!count_data_rows_fn()) {
            insert_row_fn();
        }
        check_lock_remove_fn();
        check_view_only_mode_fn();

        $table.addClass('dynamic-table-initialized');
    };

    let check_view_only_mode_fn = () => {
        if (options.view_only) {
            let $data_rows = $.dtGet(template_definitions.data_row);
            $data_rows.find('input, textarea').attr('readonly', true);
            $data_rows.find('select').attr('disabled', 'disabled');
            $data_rows.find('button').addClass('hidden');
        }
    };

    let get_row_data_fn = ($row) => {
        let row_data = {};

        for (let i = 1; i <= options.row_template_count; i++) {
            $.each($row.find('input, select, textarea, span'), function (j, el) {
                if($(el).prop("tagName") == "SPAN"){
                    if (!row_data.hasOwnProperty($(el).attr('name'))) {
                        row_data[$(el).attr('name')] = $(el).text();
                    }
                } else {
                    if ($(el).prop('type') == 'checkbox') {
                        row_data[$(el).attr('name')] = $(el).prop('checked') ? $(el).val() : 0;
                    } else {
                        row_data[$(el).attr('name')] = $(el).val();
                    }
                }
            });
            $row = $row.next();
        }
        return row_data;
    };

    let get_data_fn = (force = false) => {
        let data = [];
        let is_valid = true;

        // Get all data row
        $.each($.dtGet(template_definitions.data_row), function (i, row) {
            if (!(i % options.row_template_count)) {
                let row_data = get_row_data_fn($(row));

                if (force || options.row_validate($(row), row_data)) {
                    // Get all input on rows
                    data.push(row_data);
                } else {
                    is_valid = false;
                }
            }
        });
        if (!is_valid && options.require_all_valid) {
            return [];
        }

        return data;
    };

    let set_data_fn = (data) => {
        $.dtGet(template_definitions.data_row).remove();
        data.forEach(row_data => {
            let $row = insert_row_fn();
            $.each(row_data, function (key, value) {
                $row.find(`[name="${key}"]`).val(value);
            });
        })
    };


    let set_row_data_fn = ($row, row_data) => {
        for (let i = 0; i < options.row_template_count; i++) {
            $.each(row_data, function (key, value) {
                let field = $row.find(`[name=${key}]`);
                if (field.length) {
                    field.val(value);
                    if(field.closest('.dt-input-view-label').length) {
                        field.closest('.dt-input-view-label').find('span').html(value);
                    }
                }
            });
            $row = $row.next();
        }
    };

    let check_row_with_conditions = ($row, conditions) => {
        let row_data = get_row_data_fn($row);
        let is_correct = true;
        $.each(conditions, function (key, value) {
            if (row_data[key] != value) {
                is_correct = false;
            }
        });

        return is_correct;
    };

    let update_data_fn = (data) => {
        let $rows = $.dtGet(template_definitions.data_row);
        data.forEach((row_data) => {
            $.each($rows, (row_index, row) => {
                let $row = $(row);
                if (!(row_index % options.row_template_count)) {
                    if (check_row_with_conditions($row, row_data.conditions)) {
                        set_row_data_fn($row, row_data.updates);
                    }
                }
            });
        })
    };

    init_fn();

    return {
        getData: get_data_fn,
        setData: set_data_fn,
        updateData: update_data_fn
    }
};