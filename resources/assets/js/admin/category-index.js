$(document).ready(function () {
    let $category_table = $('#category-list-table').DataTable({
        processing: true,
        serverSide: true,
        // language: getDataTableLanguage(),
        searching: false,
        ajax: {
            url: urls['category.index']
        },
        columns: [
            {
                data: 'id',
                name: 'id',
            },
            {
                data: 'title',
                name: 'title',
                className: 'text-center',
            },
            {
                data: null,
                name: null,
                sortable: false,
                className: 'text-center',
                render: function (data, type, full, meta) {
                    return `<button class="btn btn-sm btn-default">${phrases.title.edit}</button>
                            <button class="btn btn-sm btn-danger">${phrases.title.delete}</button>`;
                }
            }
        ]
    });
});