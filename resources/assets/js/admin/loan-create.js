$(document).ready(function () {
    let $rate_table = $('.dynamic-table').dynamicTable({
        required_fields: ['rate'],
        inserted_callback: function ($row) {
            $row.find('.js-switch').each(function (i, el) {
                new Switchery(el);
            });

            $row.find('.field-use-condition').on('change', function () {
                if (!$(this).prop('checked')) {
                    $row.find('.condition-composer').find('input, select').attr('disabled', 'disabled')
                } else {
                    $row.find('.condition-composer').find('input, select').removeAttr('disabled')
                }
            });

            $row.find('.field-name-select').on('change', function () {
                let field_id = $(this).val();

                let field = loanFields[field_id];

                let $field_value_container = $row.find('.field-value-container');
                $field_value_container.html('');

                if (field && (field.type == 'select')) {
                    let $value_el = $(`<select class="form-control"></select>`);

                    $.each(field.data_source, function (value, title) {

                        $(`<option value="${value}">${title}</option>`).appendTo($value_el);
                    });

                    $value_el.appendTo($field_value_container);

                } else {
                    $(`<input type="${field.type}" class="form-control field-value" name="field_value"/>`).appendTo($field_value_container);
                }

            });
        },
        row_validate: function ($row, row_data) {
            let valid = true;
            $.each(row_data, function (key, value) {
                let $field = $row.find(`[name=${key}]`);
                if (value == '' && $field[0].hasAttribute('required')) {
                    valid = false;
                    $field.parent().addClass('has-danger');
                } else {
                    $field.parent().removeClass('has-danger');
                }
            });
            return valid;
        }
    });

    $('#loan-create-form').on('submit', function (e) {
        e.preventDefault();

        let data = {
            title: $(this).find('input[name=title]').val(),
            time_min: $(this).find('input[name=time_min]').val(),
            time_max: $(this).find('input[name=time_max]').val(),
            money_min: $(this).find('input[name=money_min]').val(),
            loan_field: [],
            conditions: $rate_table.getData()
        };

        $.each($(this).find('input[name=loan_field]:checked'), function (i, el) {
            data.loan_field.push($(el).val());
        });

        console.log(data);
        
        // TODO: call creation ajax here

    })
});