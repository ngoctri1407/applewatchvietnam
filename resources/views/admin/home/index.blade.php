@extends('admin.layouts.app')

@section('title', 'Main page')

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-6">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Summary
                        </h5>
                    </div>
                    <div class="ibox-content">
                        <div>
                            <p>Tổng cộng: <span class="money" style="font-weight: bold;color: #c71212;">{{$total}}</span></p>
                            <span style="margin-bottom: 10px">(Thành công: <span class="money" style="font-weight: bold;color: #c71212;">{{$success}}</span> , Đang đợi: <span class="money" style="font-weight: bold;color: #c71212;">{{$waiting}}</span>)</span>
                            <p>Chi phí: <span class="money" style="font-weight: bold;color: #c71212;">{{$cost}}</span></p>
                            <p>Lợi nhuận: <span id="profit" class="money" style="font-weight: bold;color: #c71212;">{{$success-$cost}}}</span> </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Cài đặt
                        </h5>
                    </div>
                    <div class="ibox-content">
                        <div>
                            <form action="{{route('settings.store')}}" method="post">
                                {{ csrf_field() }}
                                <div class="form-group row">
                                    <select class="form-control m-b" name="key">
                                        <option selected value="facebook_ads">Thêm chi phí Quảng cáo Facebook</option>
                                        <option value="amount_product">Số lượng sản phẩm</option>
                                    </select>
                                    <input class="form-control" name="value">
                                </div>

                                <button class="btn btn-sm btn-primary" type="submit">Xong</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
@endsection