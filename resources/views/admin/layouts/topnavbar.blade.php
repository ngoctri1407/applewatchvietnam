<div class="row border-bottom">
    <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
        </div>
        @if (isset($pageTitle))
            <h2 class="page-title">{!! $pageTitle !!}</h2>
        @endif
        <ul class="nav navbar-top-links navbar-right">
            <li>
                <a href="{{ url('/logout') }}">
                    <i class="fa fa-sign-out"></i> {!! __('title.logout') !!}
                </a>
            </li>
        </ul>
    </nav>
</div>
