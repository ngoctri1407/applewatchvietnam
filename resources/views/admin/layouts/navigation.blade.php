<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="block m-t-xs">
                                <strong class="font-bold">{{ Auth::user()->name }}</strong>
                                <span class="text-muted text-xs"><b class="caret"></b></span>
                            </span>
                        </span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="{{ route('home') }}">{!! __('title.home') !!}</a></li>
                        <li><a href="{{ route('logout') }}">{!! __('title.logout') !!}</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    CT
                </div>
            </li>
            <li class="{!! Request::is('admin') ? 'active' : '' !!}">
                <a href="{{ route('dashboard') }}">
                    <i class="fa fa-th-large"></i>
                    <span class="nav-label">{!! __('dashboard.dashboard') !!}</span>
                </a>
            </li>
            <li class="treeview {!! Request::is('admin/customer*') ? 'active' : '' !!}">
                <a href="{{ route('customer.index') }}">
                    <i class="fa fa-th-large"></i>
                    <span class="nav-label">{!! __('customer.manageCustomers') !!}</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="{{ isActiveRoute('customer.index') }}"><a href="{{ route('customer.index') }}">{!! __('title.list') !!}</a></li>
                </ul>
            </li>
        </ul>

    </div>
</nav>
