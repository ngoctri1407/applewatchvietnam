@extends('admin.layouts.app')

@section('title', 'Main page')

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content p-md" style="overflow: scroll">
                        <table id="customer-list-table" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>{!! __('customer.customerTitle') !!}</th>
                            <th>SĐT</th>
                            <th>Địa chỉ</th>
                            <th>Giá</th>
                            <th>Phần trăm giảm</th>
                            <th>Mã đơn hàng</th>
                            <th>Status</th>
                            <th>{!! __('title.actions') !!}</th>
                        </tr>
                        </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('DataTables/datatables.min.js') }}"></script>
    <script>
                @php
                    $phrases = \App\Common\Helpers\StringHelper::getPhrases([
                        'title'
                    ]);
                @endphp
        const phrases = @json($phrases);
        const urls =  {
            'customer.index': '{{ route("customer.index") }}',
            'customer.update': '{{ route("customer.update", ":id") }}',
            'customer.destroy': '{{ route("customer.destroy", ":id") }}'
        }
    </script>
    <script type="text/javascript" >
        $(document).ready(function () {
            let $customer_table = $('#customer-list-table').DataTable({
                processing: true,
                serverSide: true,
                // language: getDataTableLanguage(),
                searching: true,
                ajax: {
                    url: urls['customer.index']
                },
                columns: [
                    {
                        data: 'id',
                        name: 'id'
                    },
                    {
                        data: 'name',
                        name: 'name',
                        className: 'text-center',
                    },
                    {
                        data: 'phone',
                        name: 'phone',
                        className: 'text-center',
                    },
                    {
                        data: 'address',
                        name: 'address',
                        className: 'text-center',
                    },
                    {
                        data: 'price',
                        name: 'price',
                        className: 'text-center',
                        render:function (data,type,full,meta) {
                            return `<p class="text-center money" style="font-weight: bold;color: #c71212;">${data}</p>`
                        }
                    },
                    {
                        data: 'coupon',
                        name: 'coupon',
                        className: 'text-center',
                        render:function (data,type,full,meta) {
                            return `${data}%`
                        }
                    },
                    {
                        data: 'code',
                        name: 'code',
                        className: 'text-center',

                    },
                    {
                        data: 'status',
                        name: 'status',
                        className: 'text-center',
                        render:function (data,type,full,meta) {
                            if(data == 0){
                                return `<p class="text-center" style="font-weight: bold;color: #f8ac59;">Chờ xử lý</p>`
                            }else if(data == 1){
                                return `<p class="text-center" style="font-weight: bold;color: #1a7bb9;">Thành công</p>`
                            }else {
                                return `<p class="text-center" style="font-weight: bold;color: #c71212;">Thất bại</p>`
                            }
                        }
                    },
                    {
                        data: null,
                        name: null,
                        sortable: false,
                        className: 'text-center',
                        render: function (data, type, full, meta) {
                            return `<button class="btn btn-sm btn-success success-customer-button" data-id="${full.id}">Thành công</button>
                            <button class="btn btn-sm btn-danger fail-customer-button" data-id="${full.id}">Thất bại</button>
                            <button class="btn btn-sm btn-default delete-customer-button" data-id="${full.id}">${phrases.title.delete}</button>`;
                        }
                    }
                ]
            });



            $('#customer-list-table').on('click', '.success-customer-button', function () {
                let customer_id = $(this).data('id');
                swal({
                    title: phrases.title.confirm,
                    text: 'Bạn có chắc chắn muốn Xác nhận khách hàng này ?',
                    type: 'warning',
                    buttons: true
                }).then(function (value) {
                    if (value) {
                        $.ajax({
                            url: urls['customer.update'].replace(':id', customer_id),
                            data: {
                                status: 1,
                            },
                            method: 'put',
                            success: function (response) {
                                if (response.status) {
                                    swal(phrases.title.success, response.message, "success");
                                    $customer_table.ajax.reload();
                                } else {
                                    swal(phrases.title.error, response.message, "error");
                                }
                            }
                        })
                    }
                })
            });
            $('#customer-list-table').on('click', '.fail-customer-button', function () {
                let customer_id = $(this).data('id');
                swal({
                    title: phrases.title.confirm,
                    text: 'Bạn có chắc chắn muốn Xác nhận khách hàng này ?',
                    type: 'warning',
                    buttons: true
                }).then(function (value) {
                    if (value) {
                        $.ajax({
                            url: urls['customer.update'].replace(':id', customer_id),
                            data: {
                                status: 2,
                            },
                            method: 'put',
                            success: function (response) {
                                if (response.status) {
                                    swal(phrases.title.success, response.message, "success");
                                    $customer_table.ajax.reload();
                                } else {
                                    swal(phrases.title.error, response.message, "error");
                                }
                            }
                        })
                    }
                })
            });
            $('#customer-list-table').on('click', '.delete-customer-button', function () {
                let customer_id = $(this).data('id');
                swal({
                    title: phrases.title.confirm,
                    text: 'Bạn có chắc chắn muốn xóa khách hàng này ?',
                    type: 'warning',
                    buttons: true
                }).then(function (value) {
                    if (value) {
                        $.ajax({
                            url: urls['customer.destroy'].replace(':id', customer_id),
                            method: 'delete',
                            success: function (response) {
                                if (response.status) {
                                    swal(phrases.title.success, response.message, "success");
                                    $customer_table.ajax.reload();
                                } else {
                                    swal(phrases.title.error, response.message, "error");
                                }
                            }
                        })
                    }
                })
            });
        });

    </script>
@endsection
