@extends('admin.layouts.app')

@section('title', 'Main page')

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content p-md">
                        <form method="post" class="form-horizontal" id="category-create-form" action="{{ route('category.store') }}">
                            <div class="form-group"><label class="col-sm-2 control-label">{!! __('category.categoryTitle') !!}</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="title" required>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">{!! __('title.image') !!}</label>
                                <div class="col-sm-10 dropzone" id="file-dropzone">
                                    <div class="dropzone-previews"></div>
                                </div>
                                <input type="hidden" name="image">
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">{!! __('category.brands') !!}</label>
                                <div class="col-sm-10">
                                    <table class="table table-sm dynamic-table" id="brands-table">
                                        <tbody style="border-top: none">
                                        <tr class="dt-row-template">
                                            <td>
                                                <input type="text" name="brand_title" class="form-control">
                                            </td>
                                            <td width="10%">
                                                <div class="btn-group btn-group-sm inline" style="height: 38px;">
                                                    <button type="button" class="btn btn-default border dt-insert-row"><i class="fa fa-plus"></i></button>
                                                    <button type="button" class="btn btn-default border dt-remove-row"><i class="fa fa-minus"></i></button>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-white" type="submit">{!! __('title.cancel') !!}</button>
                                    <button class="btn btn-primary" type="submit">{!! __('title.save') !!}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        @php
            $phrases = \App\Common\Helpers\StringHelper::getPhrases([
                'title'
            ]);
        @endphp
        const phrases = @json($phrases);
        const urls = {
        }
    </script>
    <script type="text/javascript" src="{{ asset('js/dynamic-table.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/admin/category-create.js') }}"></script>
@endsection