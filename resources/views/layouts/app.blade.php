<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/styled.css') }}" rel="stylesheet">
</head>
<body>
@include('layouts.header')
<main class="tech-content" role="main" id="tech-content">
    <div class="tech-content__inner">
        @yield('content')
    </div>
</main>
@include('layouts.footer')
@section('scripts')
@show
</body>
</html>
