<header class="site-header" id="site-header">
    <div class="container">
        <div class="site-header__inner d-flex align-items-center justify-content-between">
            <h1 class="site-header__logo">
                <a href="/" class="d-flex">
                    <img src="{{ url('/images/logo.png') }}" alt="logo" width="122" height="24">
                </a>
            </h1>
            <div class="site-header__nav d-inline-flex align-items-center">
                <nav class="main-nav collapse navbar-collapse" id="main-nav">
                    <ul class="main-sections d-flex align-items-center">
                        <li class="item"><a href="#" class="fw-light d-block">Diễn đàn tài chính</a></li>
                        <li class="item"><a href="{{ route('about') }}" class="fw-light d-block">Về chúng tôi</a></li>
                        <li class="item"><a href="" class="fw-light d-block">Tôi cần vay</a></li>
                        <li class="item"><a href="#" class="fw-light d-block">Liên hệ</a></li>
                        <li class="item"><a href="#" class="fw-light d-block">Tải App</a></li>
                    </ul>
                </nav>
                <div class="site-header__join">
                    <a href="#" class="btn-wapper d-flex align-items-center">
						<span class="icon">
							<img src="{{ url('/images/login-btn.png') }}" alt="login" width="32" height="32">
						</span>
                        <div class="ml-h2 fz-13">
                            <span class="d-block">Đăng nhập</span>
                            <span class="d-block">Đăng ký</span>
                        </div>
                    </a>
                    <span class="visible-xs btn-toggle" data-target="#main-nav" data-toggle="collapse"><i class="fa fa-bars" aria-hidden="true"></i></span>
                </div>
            </div>
        </div>
    </div>
</header>