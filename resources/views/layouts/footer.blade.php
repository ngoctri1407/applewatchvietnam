<footer class="site-footer" id="site-footer" style="background-image: url('{{ url("/images/footer-bg.png") }}');">
    <div class="container">
        <div class="site-footer__inner d-flex align-items-center">
            <ul class="d-flex justify-content-between item-wapper w-100">
                <li class="item-inner support">
                    <p class="title">Hỗ trợ khách hàng</p>
                    <div>
                        <p class="fw-light d-flex align-items-center">
                            <span class="icon"><img src="{{ url('/images/sp-phone.png') }}" height="21" width="22"/></span>
                            <span class="text">1234 56789</span>
                        </p>
                        <p class="fw-light d-flex align-items-center">
                            <span class="icon"><img src="{{ url('/images/sp-mail.png') }}" height="24" width="27"/></span>
                            <span class="text">support@cashtech.vn</span>
                        </p>
                    </div>
                </li>
                <li class="item-inner contact">
                    <p class="title">Liên hệ</p>
                    <div>
                        <p class="fw-light d-flex align-items-center">
                            <a href=""><img src="{{ url('/images/contact-fb.png') }}" height="24" width="24"/></a>
                            <a href="" class="ml-2"><img src="{{ url('/images/contact-other.png') }}" height="25" width="33"/></a>
                            <a href="" class="ml-2"><img src="{{ url('/images/contact-zalo.png') }}" height="26" width="26"/></a>
                        </p>
                        <p class="fw-light d-flex align-items-center">
                            <span class="text">© 2013 Bản quyền thuộc về CashTech. </span>
                        </p>
                    </div>
                </li>
                <li class="item-inner download">
                    <p class="title mb-0">Công ty TNHH Cash Tech</p>
                    <p class="title">Tải ứng dụng Cash Tech </p>
                    <div>
                        <p class="fw-light d-flex align-items-center">
                            <a href=""><img src="{{ url('/images/appstore.png') }}" height="35" width="100"/></a>
                            <a href="" class="ml-2"><img src="{{ url('/images/googleplay.png') }}" height="35" width="100"/></a>
                        </p>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</footer>