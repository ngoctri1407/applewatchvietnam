<!DOCTYPE html>
<!-- saved from url=(0028) -->
<html lang="en-US" prefix="og: http://ogp.me/ns#" class="js"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><style id="stndz-style"></style>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="pingback" href="https://digitexfutures.com/xmlrpc.php">

    <script type="text/javascript" async="" src="./Kindvn - Đặt hàng thành công!_files/analytics.js.download"></script><script async="" src="./Kindvn - Đặt hàng thành công!_files/gtm.js.download"></script><script type="text/javascript" async="" src="./Kindvn - Đặt hàng thành công!_files/analytics.js.download"></script><script type="text/javascript">
        document.documentElement.className = 'js';
    </script>

    <title>Apple watch Serie 4 - Đồng hồ thông minh AppleWatch</title>
    <link rel="alternate" hreflang="en" href="https://digitexfutures.com/treasury/">

    <!-- This site is optimized with the Yoast SEO plugin v9.7 - https://yoast.com/wordpress/plugins/seo/ -->
    <link rel="canonical" href="https://digitexfutures.com/treasury/">
    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="article">
    <meta property="og:title" content="Treasury - Digitex Futures">
    <meta property="og:url" content="https://digitexfutures.com/treasury/">
    <meta property="og:site_name" content="Digitex Futures">
    <meta property="article:publisher" content="https://www.facebook.com/DigitexFutures/">
    <meta property="og:image" content="https://digitexfutures.com/wp-content/uploads/2018/10/Digitex-OG.png">
    <meta property="og:image:secure_url" content="https://digitexfutures.com/wp-content/uploads/2018/10/Digitex-OG.png">
    <meta property="og:image:width" content="650">
    <meta property="og:image:height" content="650">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Treasury - Digitex Futures">
    <meta name="twitter:site" content="@digitexfutures">
    <meta name="twitter:image" content="https://digitexfutures.com/wp-content/uploads/2018/10/Digitex-OG.png">
    <meta name="twitter:creator" content="@digitexfutures">
    <script type="application/ld+json">{"@context":"https://schema.org","@type":"Organization","url":"https://digitexfutures.com/","sameAs":["https://www.facebook.com/DigitexFutures/","https://www.instagram.com/digitexfutures","https://www.linkedin.com/company/digitex-futures-exchange/","https://www.youtube.com/channel/UCjY3JG9agHi2ePDTm6sJysw","https://twitter.com/digitexfutures"],"@id":"https://digitexfutures.com/#organization","name":"Digitex Futures","logo":"https://digitexfutures.com/wp-content/uploads/2018/10/Digitex-Icon.png"}</script>
    <!-- / Yoast SEO plugin. -->

    <link rel="dns-prefetch" href="https://fonts.googleapis.com/">
    <link rel="dns-prefetch" href="https://s.w.org/">
    <link rel="alternate" type="application/rss+xml" title="Digitex Futures » Feed" href="https://digitexfutures.com/feed/">
    <link rel="alternate" type="application/rss+xml" title="Digitex Futures » Comments Feed" href="https://digitexfutures.com/comments/feed/">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script type="text/javascript">
        window._wpemojiSettings = {"baseUrl":"https://s.w.org/images/core/emoji/11.2.0/72x72/","ext":".png","svgUrl":"https://s.w.org/images/core/emoji/11.2.0/svg/","svgExt":".svg","source":{"concatemoji":"https://digitexfutures.com/wp-includes/js/wp-emoji-release.min.js?ver=5.1.1"}};
        !function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async="" src="./Kindvn - Đặt hàng thành công!_files/js"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-137827863-1');
    </script>

    <meta content="Digitex-Child v.1.0.0" name="generator"><style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel="stylesheet" id="wp-block-library-css" href="./Kindvn - Đặt hàng thành công!_files/style.min.css" type="text/css" media="all">
    <link rel="stylesheet" id="fvp-frontend-css" href="./Kindvn - Đặt hàng thành công!_files/frontend.css" type="text/css" media="all">
    <link rel="stylesheet" id="wpml-menu-item-0-css" href="./Kindvn - Đặt hàng thành công!_files/style.css" type="text/css" media="all">
    <style id="wpml-menu-item-0-inline-css" type="text/css">
        .wpml-ls-slot-2, .wpml-ls-slot-2 a, .wpml-ls-slot-2 a:visited{background-color:#252830;color:#ffffff;}.wpml-ls-slot-2:hover, .wpml-ls-slot-2:hover a, .wpml-ls-slot-2 a:hover{color:#ffffff;background-color:#252830;}.wpml-ls-slot-2.wpml-ls-current-language, .wpml-ls-slot-2.wpml-ls-current-language a, .wpml-ls-slot-2.wpml-ls-current-language a:visited{color:#ffffff;background-color:#252830;}.wpml-ls-slot-2.wpml-ls-current-language:hover, .wpml-ls-slot-2.wpml-ls-current-language:hover a, .wpml-ls-slot-2.wpml-ls-current-language a:hover{color:#ffffff;background-color:#252830;}.wpml-ls-slot-2.wpml-ls-current-language .wpml-ls-slot-2, .wpml-ls-slot-2.wpml-ls-current-language .wpml-ls-slot-2 a, .wpml-ls-slot-2.wpml-ls-current-language .wpml-ls-slot-2 a:visited{background-color:#252830;color:#ffffff;}.wpml-ls-slot-2.wpml-ls-current-language .wpml-ls-slot-2:hover, .wpml-ls-slot-2.wpml-ls-current-language .wpml-ls-slot-2:hover a, .wpml-ls-slot-2.wpml-ls-current-language .wpml-ls-slot-2 a:hover {color:#ffffff;background-color:#252830;}
        .wpml-ls-display{display:inline!important;}
    </style>
    <link rel="stylesheet" id="parent-style-css" href="./Kindvn - Đặt hàng thành công!_files/style(1).css" type="text/css" media="all">
    <link rel="stylesheet" id="divi-style-css" href="./Kindvn - Đặt hàng thành công!_files/style(2).css" type="text/css" media="all">
    <link rel="stylesheet" id="et-builder-googlefonts-cached-css" href="./Kindvn - Đặt hàng thành công!_files/css" type="text/css" media="all">
    <link rel="stylesheet" id="dashicons-css" href="./Kindvn - Đặt hàng thành công!_files/dashicons.min.css" type="text/css" media="all">
    <link rel="stylesheet" id="divi-ultimate-blog-plugin-main-css-css" href="./Kindvn - Đặt hàng thành công!_files/main.css" type="text/css" media="all">
    <link rel="stylesheet" id="agsdcm-css" href="./Kindvn - Đặt hàng thành công!_files/divi.css" type="text/css" media="all">
    <script type="text/javascript" src="./Kindvn - Đặt hàng thành công!_files/jquery.js.download"></script>
    <script type="text/javascript" src="./Kindvn - Đặt hàng thành công!_files/jquery-migrate.min.js.download"></script>
    <script type="text/javascript" src="./Kindvn - Đặt hàng thành công!_files/jquery.fitvids.min.js.download"></script>
    <script type="text/javascript">
        /* <![CDATA[ */
        var fvpdata = {"ajaxurl":"https://digitexfutures.com/wp-admin/admin-ajax.php","nonce":"990d70cb27","fitvids":"1","dynamic":"","overlay":"","opacity":"0.75","color":"b","width":"640"};
        /* ]]> */
    </script>
    <script type="text/javascript" src="./Kindvn - Đặt hàng thành công!_files/frontend.min.js.download"></script>
    <script type="text/javascript">
        /* <![CDATA[ */
        var EXTRA = {"ajaxurl":"https://digitexfutures.com/wp-admin/admin-ajax.php","blog_feed_nonce":"0bbf463f25"};
        /* ]]> */
    </script>
    <script type="text/javascript" src="./Kindvn - Đặt hàng thành công!_files/extra.js.download"></script>
    <script type="text/javascript" src="./Kindvn - Đặt hàng thành công!_files/imagesloaded.js.download"></script>
    <link rel="https://api.w.org/" href="https://digitexfutures.com/wp-json/">
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://digitexfutures.com/xmlrpc.php?rsd">
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://digitexfutures.com/wp-includes/wlwmanifest.xml">
    <link rel="shortlink" href="https://digitexfutures.com/?p=3335">
    <link rel="alternate" type="application/json+oembed" href="https://digitexfutures.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fdigitexfutures.com%2Ftreasury%2F">
    <link rel="alternate" type="text/xml+oembed" href="https://digitexfutures.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fdigitexfutures.com%2Ftreasury%2F&amp;format=xml">
    <meta name="generator" content="WPML ver:4.2.3 stt:1,46;">
    <style type="text/css">
        .free-blog-post-header-custom .free-du-blog-1 .free-blog-post-header-content {
            padding-top: 0px!important;
            padding-bottom: 250px!important;
            max-width: 1001px!important;
        }
        .free-blog-post-header-custom .free-du-blog-1 .free-blog-post-header-content .entry-title {
            text-transform: none!important;
            font-weight: 700!important;
        }
        .free-blog-post-header-custom .free-du-blog-1 .free-blog-post-header-content .post-meta {
            text-transform: uppercase!important;
        }
        .free-blog-post-header-custom .free-du-blog-1 .free-blog-post-header-featured {
            filter: blur(0px)!important;
            -webkit-filter: blur(0px)!important;
        }
        .free-blog-post-header-custom .free-du-blog-1 .free-blog-post-header-featured-scale {
            transform: scale(1.1)!important;
        }

        @media screen and (min-width: 622px) {
            .free-blog-post-header-custom .free-du-blog-1 .free-blog-post-header-content .entry-title {
                font-size: 38px!important;
            }
        }


        .free-blog-post-featured-image-custom .free-du-blog-1 .free-blog-post-featured>* {
            margin-top: -250px!important;
            border: 4px solid #FFF!important;
        }

        .free-du-blog-1 .free-blog-related-posts-background-color {
            background: #ffffff!important;
        }
        .pull-right {
            text-align: right;
        }

        .pull-left {
            text-align: left;
        }
        p.title {
            font-size: 25px;
            margin-top: 10%;
        }
        hr {border-top: 1px solid black;}
    </style> <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"><!-- Google Tag -->
    <script type="text/javascript">(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-PSGDZT4');</script>
    <!-- End Google Tag -->
    <meta name="google-site-verification" content="WCgFxTTHGWaaRsQ8Mx0sOTZ8Zs_c5bUhHBbcH6LogdE">
    <script type="text/javascript">
        jQuery(document).ready(function($ ) {
// add '&rel=0' to end of all YouTube video URL's
// to prevent displaying related videos
            $('.nosuggestion iframe').attr( "src", function( i, val ) {
                return val + '&rel=0';
            });
        });
    </script>
    <link rel="icon" type="image/png" href="images/apple-logo.png" sizes="32x32">

    <style id="et-divi-customizer-global-cached-inline-styles">body,.et_pb_column_1_2 .et_quote_content blockquote cite,.et_pb_column_1_2 .et_link_content a.et_link_main_url,.et_pb_column_1_3 .et_quote_content blockquote cite,.et_pb_column_3_8 .et_quote_content blockquote cite,.et_pb_column_1_4 .et_quote_content blockquote cite,.et_pb_blog_grid .et_quote_content blockquote cite,.et_pb_column_1_3 .et_link_content a.et_link_main_url,.et_pb_column_3_8 .et_link_content a.et_link_main_url,.et_pb_column_1_4 .et_link_content a.et_link_main_url,.et_pb_blog_grid .et_link_content a.et_link_main_url,body .et_pb_bg_layout_light .et_pb_post p,body .et_pb_bg_layout_dark .et_pb_post p{font-size:16px}.et_pb_slide_content,.et_pb_best_value{font-size:18px}body{line-height:1.8em}.woocommerce #respond input#submit,.woocommerce-page #respond input#submit,.woocommerce #content input.button,.woocommerce-page #content input.button,.woocommerce-message,.woocommerce-error,.woocommerce-info{background:#1cb7ed!important}#et_search_icon:hover,.mobile_menu_bar:before,.mobile_menu_bar:after,.et_toggle_slide_menu:after,.et-social-icon a:hover,.et_pb_sum,.et_pb_pricing li a,.et_pb_pricing_table_button,.et_overlay:before,.entry-summary p.price ins,.woocommerce div.product span.price,.woocommerce-page div.product span.price,.woocommerce #content div.product span.price,.woocommerce-page #content div.product span.price,.woocommerce div.product p.price,.woocommerce-page div.product p.price,.woocommerce #content div.product p.price,.woocommerce-page #content div.product p.price,.et_pb_member_social_links a:hover,.woocommerce .star-rating span:before,.woocommerce-page .star-rating span:before,.et_pb_widget li a:hover,.et_pb_filterable_portfolio .et_pb_portfolio_filters li a.active,.et_pb_filterable_portfolio .et_pb_portofolio_pagination ul li a.active,.et_pb_gallery .et_pb_gallery_pagination ul li a.active,.wp-pagenavi span.current,.wp-pagenavi a:hover,.nav-single a,.posted_in a{color:#1cb7ed}.et_pb_contact_submit,.et_password_protected_form .et_submit_button,.et_pb_bg_layout_light .et_pb_newsletter_button,.comment-reply-link,.form-submit .et_pb_button,.et_pb_bg_layout_light .et_pb_promo_button,.et_pb_bg_layout_light .et_pb_more_button,.woocommerce a.button.alt,.woocommerce-page a.button.alt,.woocommerce button.button.alt,.woocommerce-page button.button.alt,.woocommerce input.button.alt,.woocommerce-page input.button.alt,.woocommerce #respond input#submit.alt,.woocommerce-page #respond input#submit.alt,.woocommerce #content input.button.alt,.woocommerce-page #content input.button.alt,.woocommerce a.button,.woocommerce-page a.button,.woocommerce button.button,.woocommerce-page button.button,.woocommerce input.button,.woocommerce-page input.button,.et_pb_contact p input[type="checkbox"]:checked+label i:before,.et_pb_bg_layout_light.et_pb_module.et_pb_button{color:#1cb7ed}.footer-widget h4{color:#1cb7ed}.et-search-form,.nav li ul,.et_mobile_menu,.footer-widget li:before,.et_pb_pricing li:before,blockquote{border-color:#1cb7ed}.et_pb_counter_amount,.et_pb_featured_table .et_pb_pricing_heading,.et_quote_content,.et_link_content,.et_audio_content,.et_pb_post_slider.et_pb_bg_layout_dark,.et_slide_in_menu_container,.et_pb_contact p input[type="radio"]:checked+label i:before{background-color:#1cb7ed}.container,.et_pb_row,.et_pb_slider .et_pb_container,.et_pb_fullwidth_section .et_pb_title_container,.et_pb_fullwidth_section .et_pb_title_featured_container,.et_pb_fullwidth_header:not(.et_pb_fullscreen) .et_pb_fullwidth_header_container{max-width:1166px}.et_boxed_layout #page-container,.et_boxed_layout.et_non_fixed_nav.et_transparent_nav #page-container #top-header,.et_boxed_layout.et_non_fixed_nav.et_transparent_nav #page-container #main-header,.et_fixed_nav.et_boxed_layout #page-container #top-header,.et_fixed_nav.et_boxed_layout #page-container #main-header,.et_boxed_layout #page-container .container,.et_boxed_layout #page-container .et_pb_row{max-width:1326px}a{color:#1cb7ed}#main-header,#main-header .nav li ul,.et-search-form,#main-header .et_mobile_menu{background-color:#252830}#top-header,#et-secondary-nav li ul{background-color:#1cb7ed}.et_header_style_centered .mobile_nav .select_page,.et_header_style_split .mobile_nav .select_page,.et_nav_text_color_light #top-menu>li>a,.et_nav_text_color_dark #top-menu>li>a,#top-menu a,.et_mobile_menu li a,.et_nav_text_color_light .et_mobile_menu li a,.et_nav_text_color_dark .et_mobile_menu li a,#et_search_icon:before,.et_search_form_container input,span.et_close_search_field:after,#et-top-navigation .et-cart-info{color:#dcdcdc}.et_search_form_container input::-moz-placeholder{color:#dcdcdc}.et_search_form_container input::-webkit-input-placeholder{color:#dcdcdc}.et_search_form_container input:-ms-input-placeholder{color:#dcdcdc}#top-menu li a{font-size:16px}body.et_vertical_nav .container.et_search_form_container .et-search-form input{font-size:16px!important}#main-footer{background-color:#252830}#footer-widgets .footer-widget a,#footer-widgets .footer-widget li a,#footer-widgets .footer-widget li a:hover{color:#ffffff}.footer-widget{color:#ffffff}#main-footer .footer-widget h4{color:#1cb7ed}.footer-widget li:before{border-color:#1cb7ed}.footer-widget .et_pb_widget div,.footer-widget .et_pb_widget ul,.footer-widget .et_pb_widget ol,.footer-widget .et_pb_widget label{line-height:1.3em}#footer-widgets .footer-widget li:before{top:7.4px}.bottom-nav,.bottom-nav a{font-size:12px}body .et_pb_button:hover,.woocommerce a.button.alt:hover,.woocommerce-page a.button.alt:hover,.woocommerce button.button.alt:hover,.woocommerce-page button.button.alt:hover,.woocommerce input.button.alt:hover,.woocommerce-page input.button.alt:hover,.woocommerce #respond input#submit.alt:hover,.woocommerce-page #respond input#submit.alt:hover,.woocommerce #content input.button.alt:hover,.woocommerce-page #content input.button.alt:hover,.woocommerce a.button:hover,.woocommerce-page a.button:hover,.woocommerce button.button:hover,.woocommerce-page button.button:hover,.woocommerce input.button:hover,.woocommerce-page input.button:hover,.woocommerce #respond input#submit:hover,.woocommerce-page #respond input#submit:hover,.woocommerce #content input.button:hover,.woocommerce-page #content input.button:hover{background:#00aded!important}h1,h2,h3,h4,h5,h6,.et_quote_content blockquote p,.et_pb_slide_description .et_pb_slide_title{line-height:1.2em}.et_slide_in_menu_container,.et_slide_in_menu_container .et-search-field{letter-spacing:px}.et_slide_in_menu_container .et-search-field::-moz-placeholder{letter-spacing:px}.et_slide_in_menu_container .et-search-field::-webkit-input-placeholder{letter-spacing:px}.et_slide_in_menu_container .et-search-field:-ms-input-placeholder{letter-spacing:px}@media only screen and (min-width:981px){.footer-widget h4{font-size:26px}.et_header_style_left #et-top-navigation,.et_header_style_split #et-top-navigation{padding:36px 0 0 0}.et_header_style_left #et-top-navigation nav>ul>li>a,.et_header_style_split #et-top-navigation nav>ul>li>a{padding-bottom:36px}.et_header_style_split .centered-inline-logo-wrap{width:71px;margin:-71px 0}.et_header_style_split .centered-inline-logo-wrap #logo{max-height:71px}.et_pb_svg_logo.et_header_style_split .centered-inline-logo-wrap #logo{height:71px}.et_header_style_centered #top-menu>li>a{padding-bottom:13px}.et_header_style_slide #et-top-navigation,.et_header_style_fullscreen #et-top-navigation{padding:27px 0 27px 0!important}.et_header_style_centered #main-header .logo_container{height:71px}#logo{max-height:50%}.et_pb_svg_logo #logo{height:50%}.et_header_style_centered.et_hide_primary_logo #main-header:not(.et-fixed-header) .logo_container,.et_header_style_centered.et_hide_fixed_logo #main-header.et-fixed-header .logo_container{height:12.78px}.et-fixed-header#top-header,.et-fixed-header#top-header #et-secondary-nav li ul{background-color:#1cb7ed}.et-fixed-header #top-menu a,.et-fixed-header #et_search_icon:before,.et-fixed-header #et_top_search .et-search-form input,.et-fixed-header .et_search_form_container input,.et-fixed-header .et_close_search_field:after,.et-fixed-header #et-top-navigation .et-cart-info{color:#dcdcdc!important}.et-fixed-header .et_search_form_container input::-moz-placeholder{color:#dcdcdc!important}.et-fixed-header .et_search_form_container input::-webkit-input-placeholder{color:#dcdcdc!important}.et-fixed-header .et_search_form_container input:-ms-input-placeholder{color:#dcdcdc!important}.et-fixed-header #top-menu li.current-menu-ancestor>a,.et-fixed-header #top-menu li.current-menu-item>a{color:#1cb7ed!important}body.home-posts #left-area .et_pb_post h2,body.archive #left-area .et_pb_post h2,body.search #left-area .et_pb_post h2{font-size:27.7333333333px}body.single .et_post_meta_wrapper h1{font-size:32px}}@media only screen and (min-width:1457px){.et_pb_row{padding:29px 0}.et_pb_section{padding:58px 0}.single.et_pb_pagebuilder_layout.et_full_width_page .et_post_meta_wrapper{padding-top:87px}.et_pb_fullwidth_section{padding:0}}h1,h1.et_pb_contact_main_title,.et_pb_title_container h1{font-size:34px}h2,.product .related h2,.et_pb_column_1_2 .et_quote_content blockquote p{font-size:29px}h3{font-size:24px}h4,.et_pb_circle_counter h3,.et_pb_number_counter h3,.et_pb_column_1_3 .et_pb_post h2,.et_pb_column_1_4 .et_pb_post h2,.et_pb_blog_grid h2,.et_pb_column_1_3 .et_quote_content blockquote p,.et_pb_column_3_8 .et_quote_content blockquote p,.et_pb_column_1_4 .et_quote_content blockquote p,.et_pb_blog_grid .et_quote_content blockquote p,.et_pb_column_1_3 .et_link_content h2,.et_pb_column_3_8 .et_link_content h2,.et_pb_column_1_4 .et_link_content h2,.et_pb_blog_grid .et_link_content h2,.et_pb_column_1_3 .et_audio_content h2,.et_pb_column_3_8 .et_audio_content h2,.et_pb_column_1_4 .et_audio_content h2,.et_pb_blog_grid .et_audio_content h2,.et_pb_column_3_8 .et_pb_audio_module_content h2,.et_pb_column_1_3 .et_pb_audio_module_content h2,.et_pb_gallery_grid .et_pb_gallery_item h3,.et_pb_portfolio_grid .et_pb_portfolio_item h2,.et_pb_filterable_portfolio_grid .et_pb_portfolio_item h2{font-size:20px}h5{font-size:18px}h6{font-size:15px}.et_pb_slide_description .et_pb_slide_title{font-size:52px}.woocommerce ul.products li.product h3,.woocommerce-page ul.products li.product h3,.et_pb_gallery_grid .et_pb_gallery_item h3,.et_pb_portfolio_grid .et_pb_portfolio_item h2,.et_pb_filterable_portfolio_grid .et_pb_portfolio_item h2,.et_pb_column_1_4 .et_pb_audio_module_content h2{font-size:18px}	h1,h2,h3,h4,h5,h6{font-family:'Fira Sans',Helvetica,Arial,Lucida,sans-serif}body,input,textarea,select{font-family:'Fira Sans',Helvetica,Arial,Lucida,sans-serif}	.et_pb_social_media_follow li a.icon{margin-right:17.1px;width:60px;height:60px}.et_pb_social_media_follow li a.icon::before{width:60px;height:60px;font-size:30px;line-height:60px}.et_pb_social_media_follow li a.follow_button{font-size:30px}body{font-weight:400;color:#333333}</style><script src="chrome-extension://mooikfkahbdckldjjndioackbalphokd/assets/prompt.js"></script><script src="./Kindvn - Đặt hàng thành công!_files/wp-emoji-release.min.js.download" type="text/javascript" defer=""></script><style>[data-columns]::before{visibility:hidden;position:absolute;font-size:1px;}</style><style id="fit-vids-style">.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}</style></head>
<body class="page-template-default page page-id-3335 free-blog-post-featured-image-show free-blog-post-featured-image-custom free-blog-post-header-custom free-blog-post-navigation-style-1 free-blog-post-header-content-center et_pb_button_helper_class et_fixed_nav et_show_nav et_cover_background et_pb_gutter windows et_pb_gutters3 et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_pb_footer_columns3 et_header_style_left et_pb_pagebuilder_layout et_right_sidebar et_divi_theme et-db et_minified_js et_minified_css chrome" style="overflow-x: hidden;">
<div id="page-container" class="et-animated-content" style="padding-top: 85px; overflow-y: hidden; margin-top: -1px;">



    <header id="main-header" data-height-onload="85" data-height-loaded="true" data-fixed-height-onload="85" class="" style="top: 0px;">
        <div class="container clearfix et_menu_container">
            <div class="logo_container">
                <span class="logo_helper"></span>
                <a href="{{route('home')}}">

                    Apple Watch Series 4

                </a>
            </div>
            <div id="et-top-navigation" data-height="71" data-fixed-height="40" style="padding-left: 183px;">
                <nav id="top-menu-nav">
                    <ul id="top-menu" class="nav"><li id="menu-item-1412" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-1412"></li>

                        <ul class="sub-menu">
                        </ul>

                        <li id="menu-item-1733" class="menu-cta menu-item menu-item-type-post_type menu-item-object-page menu-item-1733"><a href="#">Đặt mua miễn phí</a></li>
                        <li id="menu-item-wpml-ls-2-en" class="menu-item wpml-ls-slot-2 wpml-ls-item wpml-ls-item-en wpml-ls-current-language wpml-ls-menu-item wpml-ls-first-item wpml-ls-last-item menu-item-type-wpml_ls_menu_item menu-item-object-wpml_ls_menu_item menu-item-wpml-ls-2-en"><a title="English" href="#"><img class="wpml-ls-flag" src="./Kindvn - Đặt hàng thành công!_files/en.png" alt="en" title="English"><span class="wpml-ls-native">English</span></a></li>
                    </ul>						</nav>




                <div id="et_mobile_nav_menu">
                    <div class="mobile_nav closed">
                        <span class="select_page">Select Page</span>
                        <span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
                        <ul id="mobile_menu" class="et_mobile_menu"><li id="menu-item-1412" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-1412 et_first_mobile_item"></li>

                            <ul class="sub-menu">
                            </ul>

                            <li id="menu-item-1733" class="menu-cta menu-item menu-item-type-post_type menu-item-object-page menu-item-1733"><a href="#">Đặt mua miễn phí</a></li>
                            <li id="menu-item-wpml-ls-2-en" class="menu-item wpml-ls-slot-2 wpml-ls-item wpml-ls-item-en wpml-ls-current-language wpml-ls-menu-item wpml-ls-first-item wpml-ls-last-item menu-item-type-wpml_ls_menu_item menu-item-object-wpml_ls_menu_item menu-item-wpml-ls-2-en"><a title="English" href="#"><img class="wpml-ls-flag" src="./Kindvn - Đặt hàng thành công!_files/en.png" alt="en" title="English"><span class="wpml-ls-native">English</span></a></li>
                        </ul></div>
                </div>				</div> <!-- #et-top-navigation -->
        </div> <!-- .container -->
        <div class="et_search_outer">
            <div class="container et_search_form_container">
                <form role="search" method="get" class="et-search-form" action="https://digitexfutures.com/">
                    <input type="search" class="et-search-field" placeholder="Search …" value="" name="s" title="Search for:">					</form>
                <span class="et_close_search_field"></span>
            </div>
        </div>
    </header> <!-- #main-header -->
    <div id="et-main-area">

        <div id="main-content">



            <article id="post-3335" class="post-3335 page type-page status-publish hentry">


                <div class="entry-content">
                    <div id="et-boc" class="et-boc">

                        <div class="et_builder_inner_content et_pb_gutters3">
                            <div class="et_pb_section et_pb_section_0 et_pb_with_background et_pb_section_parallax et_section_regular section_has_divider et_pb_bottom_divider">

                                <div class="et_parallax_bg et_pb_parallax_css" style="background-image: url('./Beats Solo3 Wireless - Tai nghe không dây chất lượng cao _ 3K Sho_files/3kshop_beats_solo3_satin.jpg');"></div>


                                <div class="et_pb_with_border et_pb_row et_pb_row_1">
                                    <div class="et_pb_column et_pb_column_4_4 et_pb_column_0    et_pb_css_mix_blend_mode_passthrough et-last-child">


                                        <div class="et_pb_module et_pb_text et_pb_text_0 et_pb_bg_layout_dark  et_pb_text_align_center">


                                            <div class="et_pb_text_inner">
                                                <h2 style="text-align: center;">Đặt hàng thành công !</h2>
                                            </div>
                                        </div> <!-- .et_pb_text --><div class="et_pb_module et_pb_code et_pb_code_0 et_had_animation" style="">


                                            <div class="et_pb_code_inner">
                                                <p id="treasury_price" style="font-size: 31px; color: #1db6ed; text-align: center;">Mã đơn hàng: "<strong><a id="randomstring">{{$code}}</a></strong>"

                                                </p></div> <!-- .et_pb_code_inner -->
                                        </div> <!-- .et_pb_code --><div class="et_pb_module et_pb_code et_pb_code_1 et_had_animation" style="">


                                            <div class="et_pb_code_inner">
                                                <p id="dgtx_left">

                                                    <script type="text/javascript">
                                                        jQuery('#dgtx_left').load('https://digitexfutures.com/treasury_number_of_dgtx_left_in_contract.html?nocache=' + (new Date()).getTime()); // we add the time onto this call so that browsers see this as the first time theyve called this file and will not cache the result...)
                                                        console.log("calling dgtx left update");


                                                        setInterval(function(){
                                                            jQuery('#dgtx_left').load('https://digitexfutures.com/treasury_number_of_dgtx_left_in_contract.html?nocache=' + (new Date()).getTime()); // we add the time onto this call so that browsers see this as the first time theyve called this file and will not cache the result...)
                                                            console.log("repeating dgtx left update");
                                                        }, 2000) /* time in milliseconds (ie 5 seconds)*/
                                                    </script>
                                                </p><p style="color: white; font-size: 10pt; text-align: left;">Đơn hàng của bạn sắp hoàn thành! Vui lòng thanh toán chi phí vận chuyển của bạn.
                                                </p></div> <!-- .et_pb_code_inner -->
                                        </div> <!-- .et_pb_code --><div class="et_pb_module et_pb_code et_pb_code_2">


                                            <div class="et_pb_code_inner">
                                                <!-- bar counter code -->
                                                <!-- .et_pb_code -->
                                                <ul id="percent_bar" class="et_pb_module et_pb_counters et_pb_counters_0 et-waypoint et_pb_bg_layout_light et-animated"><li class="et_pb_counter_0 et_pb_text_align_center"><span class="et_pb_counter_container et-animated"><span id="the_bar" class="et_pb_counter_amount" style="width:46%; padding: 10px 0px;" data-width="46%"><span class="et_pb_counter_amount_number"></span></span><span id="the_bar1" class="et_pb_counter_amount overlay" style="width:46%" data-width="46%"><span class="et_pb_counter_amount_number"></span></span></span></li></ul>
                                                <script type="text/javascript">
                                                    jQuery('#percent_bar').load('https://digitexfutures.com/treasury_number_of_dgtx_left_in_contract_as_percentage.txt?nocache=' + (new Date()).getTime()); // we add the time onto this call so that browsers see this as the first time theyve called this file and will not cache the result...)
                                                    console.log("calling dgtx left percentage");

                                                    setInterval(function(){
                                                        jQuery('#percent_bar').load('https://digitexfutures.com/treasury_number_of_dgtx_left_in_contract_as_percentage.txt?nocache=' + (new Date()).getTime()); // we add the time onto this call so that browsers see this as the first time theyve called this file and will not cache the result...)
                                                        console.log("repeating dgtx left percentage");
                                                    }, 300000) /* time in milliseconds (ie 300 seconds)*/
                                                </script>
                                            </div> <!-- .et_pb_code_inner -->
                                        </div> <!-- .et_pb_code --><div class="et_pb_module et_pb_code et_pb_code_3">


                                            <div class="et_pb_code_inner">

                                                <script type="text/javascript">
                                                    jQuery('#price_update').load('https://digitexfutures.com/treasury_futureRate_time_and_price.txt?nocache=' + (new Date()).getTime()); // we add the time onto this call so that browsers see this as the first time theyve called this file and will not cache the result...)
                                                    console.log("calling price update");

                                                    setInterval(function(){
                                                        jQuery('#price_update').load('https://digitexfutures.com/treasury_futureRate_time_and_price.txt?nocache=' + (new Date()).getTime()); // we add the time onto this call so that browsers see this as the first time theyve called this file and will not cache the result...)
                                                        console.log("repeating price update");
                                                    }, 2020) /* time in milliseconds (ie 2 seconds)*/

                                                </script>
                                            </div> <!-- .et_pb_code_inner -->
                                        </div> <!-- .et_pb_code -->
                                    </div> <!-- .et_pb_column -->


                                </div> <!-- .et_pb_row -->

                                <div class="et_pb_bottom_inside_divider"></div>
                            </div> <!-- .et_pb_section --><div class="et_pb_section et_pb_section_1 et_section_regular">







                            </div> <!-- .et_pb_section --><div class="et_pb_section et_pb_section_2 et_pb_with_background et_section_regular section_has_divider et_pb_bottom_divider">




                                <div class="et_pb_row et_pb_row_3">
                                    <div class="et_pb_column et_pb_column_2_3 et_pb_column_2    et_pb_css_mix_blend_mode_passthrough">


                                        <div class="et_pb_module et_pb_text et_pb_text_2 et_pb_bg_layout_light  et_pb_text_align_center">
                                            <h2>Thông tin đơn hàng</h2>
                                            <div class="row" style="
    border-bottom: 1px solid black;
">
                                                <span class="col-xs-3"><img src="{{$link_img}}"></span>
                                                <span class="col-xs-9 pull-left"><p class="title">Đồng Hồ Thông Minh Apple Watch Series 4 {{$customer->model}} {{$customer->color}}</p></span>
                                            </div>
                                            <div class="row">
                                                <span class="col-xs-8 pull-left"><p>Tên khách hàng: </p></span>
                                                <span class="col-xs-4 pull-right"><p>{{$customer->name}}</p></span>
                                            </div>
                                            <div class="row">
                                                <span class="col-xs-8 pull-left"><p>Địa chỉ giao hàng: </p></span>
                                                <span class="col-xs-4 pull-right"><p>{{$customer->address}}</p></span>
                                            </div>
                                            <div class="row" style="
    border-bottom: 1px solid black;
">
                                                <span class="col-xs-8 pull-left"><p>Số điện thoại: </p></span>
                                                <span class="col-xs-4 pull-right"><p>{{$customer->phone}}</p></span>
                                            </div>

                                            <div class="row">
                                                <span class="col-xs-8 pull-left"><p>Đồng Hồ Thông Minh Apple Watch Series 4 {{$customer->model}} {{$customer->color}}</p></span>
                                                <span class="col-xs-4 pull-right"><p class="money">{{$customer->price}}</p></span>
                                            </div>
                                            <div class="row">
                                                <span class="col-xs-8 pull-left"><p>Phí vận chuyển</p></span>
                                                <span class="col-xs-4 pull-right"><p>80.000 VNĐ</p></span>
                                            </div>
                                            <div class="row">
                                                <span class="col-xs-8 pull-left"><p>Phí bảo hành</p></span>
                                                <span class="col-xs-4 pull-right"><p>Miễn phí (1 năm)</p></span>
                                            </div>
                                        <hr/>
                                            <div class="row">
                                                <span class="col-xs-8 pull-left"><p>Tổng cộng</p></span>
                                                <span class="col-xs-4 pull-right"><p class="money">{{$customer->price + 80000}}</p></span>
                                            </div>
                                        </div> <!-- .et_pb_text --><div class="et_pb_button_module_wrapper et_pb_button_1_wrapper et_pb_button_alignment_center et_pb_module ">

                                        </div>
                                    </div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_3 et_pb_column_3    et_pb_css_mix_blend_mode_passthrough">


                                        <div class="et_pb_module et_pb_text et_pb_text_2 et_pb_bg_layout_light  et_pb_text_align_center">


                                            <div class="et_pb_text_inner">
                                                <h2 style="text-align: center;">Thông tin thanh toán</h2>
                                                <h3 style="text-align: center; margin-bottom: 15px;">Khách hàng vui lòng chuyển khoản giá trị đơn hàng của mình theo thông tin thanh toán sau đây:</h3>

                                                <p style="text-align: left;">Đại diện công ty công nghệ Apple VietNam
                                                    <br>SACOMBANK - TP Hồ Chí Minh - Chi nhánh Tân Phú
                                                    <br>Chủ TK: <strong>PHẠM NGỌC TRÍ </strong>
                                                    <br>Số TK: <strong> 0602 0595 3194  </strong>
                                                    <br>Nội dung chuyển khoản vui lòng ghi mã đơn hàng của bạn: "<strong><a id="randomstring1">{{$code}}</a></strong>"
                                                    <br><strong>Lưu ý:</strong> Khi chuyển khoản, Quý khách phải ghi rõ nội dung là mã đơn hàng của mình.
                                                    <br><span style="font-weight: bold;">Apple VietNam</span> sẽ gửi hàng cho bạn từ 1 - 3 ngày làm việc, sau khi nhận được thanh toán của bạn. 				</p></div>
                                        </div>
                                    </div> <!-- .et_pb_column -->


                                </div> <!-- .et_pb_row -->


                                <div class="et_pb_bottom_inside_divider"></div>
                            </div> <!-- .et_pb_section --><div class="et_pb_section et_pb_section_3 et_section_regular section_has_divider et_pb_bottom_divider">

                                <div class="et_pb_module et_pb_text et_pb_text_1 et_pb_bg_layout_light  et_pb_text_align_center">

                                    <div class="et_pb_text_inner">
                                        <h3> <span style="font-weight: bold;">Apple VietNam</span> chỉ gửi hàng đi khi nhận được thanh toán của Quý khách. Xin cảm ơn! </h3>
                                    </div>

                                </div> <!-- .et_pb_text -->


                                <div class="et_pb_row et_pb_row_4">
                                    <div class="et_pb_column et_pb_column_4_4 et_pb_column_4    et_pb_css_mix_blend_mode_passthrough et-last-child et_pb_column_empty">



                                    </div> <!-- .et_pb_column -->


                                </div> <!-- .et_pb_row -->

                                <div class="et_pb_bottom_inside_divider"></div>
                            </div> <!-- .et_pb_section -->			</div>

                    </div>					</div> <!-- .entry-content -->


            </article> <!-- .et_pb_post -->



        </div> <!-- #main-content -->


        <span class="et_pb_scroll_top et-pb-icon et-hidden" style="display: inline;"></span>


        <footer id="main-footer">




            <div id="footer-bottom">
                <div class="container clearfix">
                    <div id="footer-info">Được tài trợ độc quyền bởi Apple. <a href="https://digitexfutures.com/terms" rel="nofollow">Terms &amp; Conditions</a></div>					</div>	<!-- .container -->
            </div>
        </footer> <!-- #main-footer -->
    </div> <!-- #et-main-area -->


</div> <!-- #page-container -->

<script type="text/javascript">
    var et_animation_data = [{"class":"et_pb_code_0","style":"slide","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"50%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_code_1","style":"zoom","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"50%","starting_opacity":"0%","speed_curve":"ease-in-out"}];
</script>
<!-- Google Tag (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PSGDZT4" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag (noscript) -->
<script type="text/javascript">
    jQuery(function ($) {
        // go to video div on button click
        $('a.video').on('click', function(event){
            $('#intro_video').click();
        });
    });
</script><script type="text/javascript">
    /* <![CDATA[ */
    var DIVI = {"item_count":"%d Item","items_count":"%d Items"};
    var et_shortcodes_strings = {"previous":"Previous","next":"Next"};
    var et_pb_custom = {"ajaxurl":"https://digitexfutures.com/wp-admin/admin-ajax.php","images_uri":"https://digitexfutures.com/wp-content/themes/Divi/images","builder_images_uri":"https://digitexfutures.com/wp-content/themes/Divi/includes/builder/images","et_frontend_nonce":"308c7eac9b","subscription_failed":"Please, check the fields below to make sure you entered the correct information.","et_ab_log_nonce":"0ccc239af5","fill_message":"Please, fill in the following fields:","contact_error_message":"Please, fix the following errors:","invalid":"Invalid email","captcha":"Captcha","prev":"Prev","previous":"Previous","next":"Next","wrong_captcha":"You entered the wrong number in captcha.","ignore_waypoints":"no","is_divi_theme_used":"1","widget_search_selector":".widget_search","is_ab_testing_active":"","page_id":"3335","unique_test_id":"","ab_bounce_rate":"5","is_cache_plugin_active":"no","is_shortcode_tracking":"","tinymce_uri":""};
    var et_pb_box_shadow_elements = [];
    /* ]]> */
</script>
<script type="text/javascript" src="./Kindvn - Đặt hàng thành công!_files/custom.min.js.download"></script>
<script type="text/javascript" src="./Kindvn - Đặt hàng thành công!_files/common.js.download"></script>
<script type="text/javascript" src="./Kindvn - Đặt hàng thành công!_files/wp-embed.min.js.download"></script>
<style id="et-builder-module-design-cached-inline-styles">div.et_pb_section.et_pb_section_0{background-image:linear-gradient(130deg,#1cb7ed 0%,#7b32e8 100%)!important}.et_pb_section_3.section_has_divider.et_pb_bottom_divider .et_pb_bottom_inside_divider{background-image:url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSIyNTBweCIgdmlld0JveD0iMCAwIDEyODAgMTQwIiBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJub25lIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxnIGZpbGw9IiNmZmZmZmYiPjxwYXRoIGQ9Ik0wIDE0MGgxMjgwQzU3My4wOCAxNDAgMCAwIDAgMHoiLz48L2c+PC9zdmc+);background-size:100% 250px;bottom:0;height:250px;z-index:1}.et_pb_text_2 h2{font-family:'Roboto',Helvetica,Arial,Lucida,sans-serif;font-weight:700;font-size:30px;line-height:1.2em}.et_pb_section_0{padding-top:7vw;padding-bottom:10vw}.et_pb_button_1_wrapper .et_pb_button_1,.et_pb_button_1_wrapper .et_pb_button_1:hover{padding-top:15px!important;padding-bottom:15px!important}.et_pb_button_1_wrapper{margin-top:30px!important;margin-bottom:10px!important}body #page-container .et_pb_button_1{color:#252830!important;border-width:2px!important;border-color:#ffffff;border-radius:0px;font-size:20px;font-family:'Fira Sans',Helvetica,Arial,Lucida,sans-serif!important;font-weight:700!important;background-color:#13c730}body #page-container .et_pb_button_1,body #page-container .et_pb_button_1:hover{padding-right:1em!important;padding-left:1em!important}body #page-container .et_pb_button_1:before,body #page-container .et_pb_button_1:after{display:none!important}.et_pb_button_1{box-shadow:6px 6px 18px 0px #dcdcdc}.et_pb_button_1,.et_pb_button_1:after{transition:all 300ms ease 0ms}.et_pb_image_0{margin-left:0}.et_pb_section_3{border-radius:10px 10px 10px 10px;overflow:hidden}.et_pb_section_4{padding-top:0px;padding-bottom:0px;margin-top:0px;margin-bottom:0px}.et_pb_text_2 p{line-height:2em}.et_pb_section_4.et_pb_section{background-color:#252830!important}.et_pb_section_4.section_has_divider.et_pb_top_divider .et_pb_top_inside_divider{background-image:url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSIyNTBweCIgdmlld0JveD0iMCAwIDEyODAgMTQwIiBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJub25lIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxnIGZpbGw9IiNmZmZmZmYiPjxwYXRoIGQ9Ik0wIDQ3LjQ0TDE3MCAwbDYyNi40OCA5NC44OUwxMTEwIDg3LjExbDE3MC0zOS42N1YxNDBIMFY0Ny40NHoiIGZpbGwtb3BhY2l0eT0iLjUiLz48cGF0aCBkPSJNMCA5MC43MmwxNDAtMjguMjggMzE1LjUyIDI0LjE0TDc5Ni40OCA2NS44IDExNDAgMTA0Ljg5bDE0MC0xNC4xN1YxNDBIMFY5MC43MnoiLz48L2c+PC9zdmc+);background-size:100% 250px;top:0;height:250px;z-index:1;transform:rotateX(180deg)}.et_pb_image_1{max-width:55%;text-align:center}.et_pb_text_3.et_pb_text{color:#e2e2e2!important}.et_pb_text_3 p{line-height:1.6em}.et_pb_text_3{font-size:14px;line-height:1.6em}.et_pb_text_3 h3{color:#ffffff!important}.et_pb_social_media_follow li.et_pb_social_media_follow_network_0 a{width:auto;height:auto}.et_pb_social_media_follow li.et_pb_social_media_follow_network_1 a{width:auto;height:auto}.et_pb_social_media_follow li.et_pb_social_media_follow_network_2 a{width:auto;height:auto}.et_pb_social_media_follow li.et_pb_social_media_follow_network_3 a{width:auto;height:auto}.et_pb_social_media_follow li.et_pb_social_media_follow_network_4 a{width:auto;height:auto}.et_pb_text_2{font-size:18px;line-height:2em;max-width:800px}.et_pb_text_2 h3{color:#1cb7ed!important}.et_pb_section_2.section_has_divider.et_pb_bottom_divider .et_pb_bottom_inside_divider{background-image:url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSIyMDBweCIgdmlld0JveD0iMCAwIDEyODAgMTQwIiBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJub25lIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxnIGZpbGw9IiNmZmZmZmYiPjxwYXRoIGQ9Ik0wIDE0MGgxMjgwQzU3My4wOCAxNDAgMCAwIDAgMHoiLz48L2c+PC9zdmc+);background-size:100% 200px;bottom:0;height:200px;z-index:1;transform:rotateY(180deg)}body #page-container .et_pb_button_0,body #page-container .et_pb_button_0:hover{padding-right:1em!important;padding-left:1em!important}.et_pb_section_0.section_has_divider.et_pb_bottom_divider .et_pb_bottom_inside_divider{background-image:url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSIyNTBweCIgdmlld0JveD0iMCAwIDEyODAgMTQwIiBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJub25lIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxnIGZpbGw9IiNmZmZmZmYiPjxwYXRoIGQ9Ik0wIDQ3LjQ0TDE3MCAwbDYyNi40OCA5NC44OUwxMTEwIDg3LjExbDE3MC0zOS42N1YxNDBIMFY0Ny40NHoiIGZpbGwtb3BhY2l0eT0iLjUiLz48cGF0aCBkPSJNMCA5MC43MmwxNDAtMjguMjggMzE1LjUyIDI0LjE0TDc5Ni40OCA2NS44IDExNDAgMTA0Ljg5bDE0MC0xNC4xN1YxNDBIMFY5MC43MnoiLz48L2c+PC9zdmc+);background-size:100% 250px;bottom:0;height:250px;z-index:1;transform:rotateY(180deg)}.et_pb_row_0{background-color:rgba(37,40,48,0.67);border-width:1px;border-color:#252830;box-shadow:0px 2px 18px 0px #252830}.et_pb_row_1{background-color:rgba(37,40,48,0.67);border-width:1px;border-color:#252830;box-shadow:0px 2px 18px 0px #252830}.et_pb_row_1,.et_pb_pagebuilder_layout.single.et_full_width_page #page-container .et_pb_row_1{max-width:600px!important}.et_pb_column_0{padding-top:20px;padding-right:20px;padding-bottom:20px;padding-left:20px}.et_pb_text_0{font-family:'Fira Sans',Helvetica,Arial,Lucida,sans-serif}.et_pb_text_0 h1{font-family:'Fira Sans',Helvetica,Arial,Lucida,sans-serif;font-weight:700;font-size:58px;line-height:1.2em;text-align:center}.et_pb_text_0 h3{font-family:'Fira Sans',Helvetica,Arial,Lucida,sans-serif;font-weight:700;text-transform:uppercase;font-size:24px;color:#ffffff!important;letter-spacing:10px;line-height:2em;text-align:center}.et_pb_code_0{padding-top:0px;padding-bottom:1px}.et_pb_button_0_wrapper .et_pb_button_0,.et_pb_button_0_wrapper .et_pb_button_0:hover{padding-top:15px!important;padding-bottom:15px!important}.et_pb_section_2.et_pb_section{background-color:#f2f2fa!important}body #page-container .et_pb_button_0{color:#252830!important;border-width:2px!important;border-color:#ffffff;border-radius:0px;font-size:20px;font-family:'Fira Sans',Helvetica,Arial,Lucida,sans-serif!important;font-weight:700!important;background-color:#13c730}.et_pb_button_0_wrapper{margin-top:30px!important;margin-bottom:10px!important}body #page-container .et_pb_button_0:before,body #page-container .et_pb_button_0:after{display:none!important}.et_pb_text_1 p{line-height:2em}div.et_pb_section.et_pb_section_2{background-image:linear-gradient(180deg,#ffffff 0%,#f2f2fa 50%)!important}.et_pb_text_1 h3{font-weight:300;font-size:32px;color:#1cb7ed!important;line-height:1.5em}.et_pb_button_0{box-shadow:6px 6px 18px 0px #252830}.et_pb_text_1{font-family:'Fira Sans',Helvetica,Arial,Lucida,sans-serif;line-height:2em;max-width:1000px;padding-top:0px!important;padding-right:0px!important;padding-left:0px!important;margin-top:0px!important;margin-right:0px!important;margin-left:0px!important}.et_pb_text_1 h2{font-family:'Roboto',Helvetica,Arial,Lucida,sans-serif;font-weight:700;font-size:45px;line-height:1.2em}.et_pb_column_1{padding-top:0px}.et_pb_row_2.et_pb_row{margin-top:0px!important;padding-top:0px}.et_pb_section_1{border-radius:10px 10px 10px 10px;overflow:hidden;padding-top:0px;padding-bottom:0px;margin-top:0px;margin-bottom:0px}.et_pb_button_0,.et_pb_button_0:after{transition:all 300ms ease 0ms}ul.et_pb_social_media_follow_0 a.icon{border-radius:34px 34px 34px 34px}.et_pb_social_media_follow_network_0 a.icon{background-color:#4267b2!important}.et_pb_social_media_follow_network_1 a.icon{background-color:#48aae6!important}.et_pb_social_media_follow_network_2 a.icon{background-color:#ff0000!important}.et_pb_social_media_follow_network_3 a.icon{background-color:#0077b5!important}.et_pb_social_media_follow_network_4 a.icon{background-image:linear-gradient(180deg,#e21d1d 0%,#ba28c1 100%)!important;background-color:#fcb6ab!important}.et_pb_image_1.et_pb_module{margin-left:auto!important;margin-right:auto!important}.et_pb_text_2.et_pb_module{margin-left:auto!important;margin-right:auto!important}.et_pb_text_1.et_pb_module{margin-left:auto!important;margin-right:auto!important}@media only screen and (max-width:980px){.et_pb_section_0{padding-top:100px;padding-bottom:100px}.et_pb_section_0.section_has_divider.et_pb_bottom_divider .et_pb_bottom_inside_divider{background-image:url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSIxNTBweCIgdmlld0JveD0iMCAwIDEyODAgMTQwIiBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJub25lIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxnIGZpbGw9IiNmZmZmZmYiPjxwYXRoIGQ9Ik0wIDQ3LjQ0TDE3MCAwbDYyNi40OCA5NC44OUwxMTEwIDg3LjExbDE3MC0zOS42N1YxNDBIMFY0Ny40NHoiIGZpbGwtb3BhY2l0eT0iLjUiLz48cGF0aCBkPSJNMCA5MC43MmwxNDAtMjguMjggMzE1LjUyIDI0LjE0TDc5Ni40OCA2NS44IDExNDAgMTA0Ljg5bDE0MC0xNC4xN1YxNDBIMFY5MC43MnoiLz48L2c+PC9zdmc+);background-size:100% 150px;bottom:0;height:150px;z-index:1;transform:rotateY(180deg)}.et_pb_text_0 h1{font-size:40px}.et_pb_text_0 h3{font-size:16px;letter-spacing:5px}.et_pb_text_1 h2{font-size:35px}.et_pb_text_2 h2{font-size:30px}.et_pb_section_3.section_has_divider.et_pb_bottom_divider .et_pb_bottom_inside_divider{background-image:url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSIxNTBweCIgdmlld0JveD0iMCAwIDEyODAgMTQwIiBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJub25lIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxnIGZpbGw9IiNmZmZmZmYiPjxwYXRoIGQ9Ik0wIDE0MGgxMjgwQzU3My4wOCAxNDAgMCAwIDAgMHoiLz48L2c+PC9zdmc+);background-size:100% 150px;bottom:0;height:150px;z-index:1}.et_pb_section_4{padding-top:100px;padding-bottom:100px}.et_pb_section_4.section_has_divider.et_pb_top_divider .et_pb_top_inside_divider{background-image:url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSIxNTBweCIgdmlld0JveD0iMCAwIDEyODAgMTQwIiBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJub25lIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxnIGZpbGw9IiNmZmZmZmYiPjxwYXRoIGQ9Ik0wIDQ3LjQ0TDE3MCAwbDYyNi40OCA5NC44OUwxMTEwIDg3LjExbDE3MC0zOS42N1YxNDBIMFY0Ny40NHoiIGZpbGwtb3BhY2l0eT0iLjUiLz48cGF0aCBkPSJNMCA5MC43MmwxNDAtMjguMjggMzE1LjUyIDI0LjE0TDc5Ni40OCA2NS44IDExNDAgMTA0Ljg5bDE0MC0xNC4xN1YxNDBIMFY5MC43MnoiLz48L2c+PC9zdmc+);background-size:100% 150px;top:0;height:150px;z-index:1;transform:rotateX(180deg)}}@media only screen and (max-width:767px){.et_pb_section_0.section_has_divider.et_pb_bottom_divider .et_pb_bottom_inside_divider{background-image:url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSIxMTBweCIgdmlld0JveD0iMCAwIDEyODAgMTQwIiBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJub25lIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxnIGZpbGw9IiNmZmZmZmYiPjxwYXRoIGQ9Ik0wIDQ3LjQ0TDE3MCAwbDYyNi40OCA5NC44OUwxMTEwIDg3LjExbDE3MC0zOS42N1YxNDBIMFY0Ny40NHoiIGZpbGwtb3BhY2l0eT0iLjUiLz48cGF0aCBkPSJNMCA5MC43MmwxNDAtMjguMjggMzE1LjUyIDI0LjE0TDc5Ni40OCA2NS44IDExNDAgMTA0Ljg5bDE0MC0xNC4xN1YxNDBIMFY5MC43MnoiLz48L2c+PC9zdmc+);background-size:100% 110px;bottom:0;height:110px;z-index:1;transform:rotateY(180deg)}.et_pb_text_0 h1{font-size:37px}.et_pb_text_0 h3{font-size:14px}.et_pb_text_1 h2{font-size:28px}.et_pb_section_2{padding-top:0px;margin-top:0px}.et_pb_text_2 h2{font-size:28px}.et_pb_section_3.section_has_divider.et_pb_bottom_divider .et_pb_bottom_inside_divider{background-image:url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSIxMTBweCIgdmlld0JveD0iMCAwIDEyODAgMTQwIiBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJub25lIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxnIGZpbGw9IiNmZmZmZmYiPjxwYXRoIGQ9Ik0wIDE0MGgxMjgwQzU3My4wOCAxNDAgMCAwIDAgMHoiLz48L2c+PC9zdmc+);background-size:100% 110px;bottom:0;height:110px;z-index:1}.et_pb_section_4.section_has_divider.et_pb_top_divider .et_pb_top_inside_divider{background-image:url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSIxMTBweCIgdmlld0JveD0iMCAwIDEyODAgMTQwIiBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJub25lIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxnIGZpbGw9IiNmZmZmZmYiPjxwYXRoIGQ9Ik0wIDQ3LjQ0TDE3MCAwbDYyNi40OCA5NC44OUwxMTEwIDg3LjExbDE3MC0zOS42N1YxNDBIMFY0Ny40NHoiIGZpbGwtb3BhY2l0eT0iLjUiLz48cGF0aCBkPSJNMCA5MC43MmwxNDAtMjguMjggMzE1LjUyIDI0LjE0TDc5Ni40OCA2NS44IDExNDAgMTA0Ljg5bDE0MC0xNC4xN1YxNDBIMFY5MC43MnoiLz48L2c+PC9zdmc+);background-size:100% 110px;top:0;height:110px;z-index:1;transform:rotateX(180deg)}}</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.money').mask('000.000.000.000.000 VNĐ', {reverse: true});
    })
</script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>

<img alt="preload image" style="display: none;"><img alt="preload image" style="display: none;"></body></html>