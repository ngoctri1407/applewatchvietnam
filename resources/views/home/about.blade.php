@extends('layouts.app')

@section('title', 'Main page')

@section('content')
    <div class="tech-about">
        <section>
            <div class="tech-about__banner"
                 style="background-image: url('{{ url('/images/about-banner.png') }}');"></div>
        </section>
        <div class="container">
            <section class="tech-about__content">
                <div class="row">
                    <div class="col-sm-6">
                        <p class="title">Giới thiệu về Cash Tech</p>
                        <div>
                            <p class="mb-0">CashTech được thành lập và chính thức đi vào hoạt động với số vốn ban
                                đầu là 152 tỷ đồng. Dựa trên nền tảng công nghệ tiên tiến và trình độ quản lý chuyên
                                sâu,
                                chúng tôi luôn tiên phong trong các xu hướng dịch vụ tài chính hiện đại, hướng tới
                                mục tiêu trở thành Quỹ tín dụng tài chính số một tại Việt Nam.</p>
                            <p class="mb-0">Trong hơn 8 năm hoạt động, CashTech hiện tại đã và đang cung cấp đa dạng
                                các gói vay tín chấp, hỗ trợ đa đối tượng, bất kì ai cũng có thể vay, góp phần giải
                                quyết gánh nặng tài chính đột xuất, đáp ứng nhu cầu cá nhân, nâng cao đời sống </p>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <img src="{{ url('/images/about-content.png') }}"/></div>
                </div>
            </section>
            <center class="my-4">
                <div class="linear-title">
                    CAM KẾT CỦA CHÚNG TÔI
                </div>
            </center>
            <section class="tech-about__promis">
                <p class="text-center title">Ngoài khẳng định mang đến cho người tiêu dùng trải nghiệm " Vay Tiêu
                    Dùng Tín Chấp " đáng tin cậy và nhanh chóng nhất, CashTech còn được mở rộng hơn,
                    hướng đến những giá trị khác:
                </p>
                <ul>
                    <li>
                        <div class="inner">
                            <img src="{{ url('/images/about-customer.png') }}" height="54" width="68"/>
                            <p class="mb-0 mt-1 sub-title">Khách hàng là trọng tâm</p>
                            <p class="mb-0 text-center">
                                Cam kết mang lại gói vay phù hợp cho bất kì khách hàng nào có nhu cầu.Thỏa mãn khách
                                hàng là tôn chỉ được đặt lên hàng đầu.
                                Thấu hiểu và thân thiện.
                            </p>
                        </div>
                    </li>
                    <li>
                        <div class="inner">
                            <img src="{{ url('/images/about-professional.png') }}" height="62" width="62"/>
                            <p class="mb-0 mt-1 sub-title">Chuyên nghiệp</p>
                            <p class="mb-0 text-center">
                                Thể chế, điều lệ và lãi suất minh bạch. Nghiệp vụ chuyên môn vững vàng. Văn hóa
                                ứng xử chuẩn mực.
                            </p>
                        </div>
                    </li>
                    <li>
                        <div class="inner">
                            <img src="{{ url('/images/about-speed.png') }}" height="50" width="48"/>
                            <p class="mb-0 mt-1 sub-title">Tốc độ</p>
                            <p class="mb-0 text-center">
                                Hồ sơ, thủ tục đơn giản. Quy trình nhanh chóng, tiết kiệm tối đa thời gian. Thẩm
                                định chính xác và hiệu quả.
                            </p>
                        </div>
                    </li>
                    <li>
                        <div class="inner">
                            <img src="{{ url('/images/about-creative.png') }}" height="60" width="58"/>
                            <p class="mb-0 mt-1 sub-title">Sáng tạo</p>
                            <p class="mb-0 text-center">
                                Liên tục cải tiến các gói vay. Sản phẩm, dịch vụ khác biệt. Nhiều sự lựa chọn tối ưu
                                nhất cho khách hàng.
                            </p>
                        </div>
                    </li>
                    <li>
                        <div class="inner">
                            <img src="{{ url('/images/about-partner.png') }}" height="56" width="58"/>
                            <p class="mb-0 mt-1 sub-title">Đối tác</p>
                            <p class="mb-0 text-center">
                                Không ngừng mở rộng mọi cơ hội đầu tư.
                                Luôn tôn trọng, đề cao, hợp tác bền vững
                                với các đối tác của mình.
                            </p>
                        </div>
                    </li>
                    <li>
                        <div class="inner">
                            <img src="{{ url('/images/about-investor.png') }}" height="56" width="56"/>
                            <p class="mb-0 mt-1 sub-title">Nhà đầu tư</p>
                            <p class="mb-0 text-center">
                                Đề cao mục tiêu hiệu quả trong tất cả các hoạt động vận hành kinh doanh. Giữ vững sự
                                tập trung, đưa CashTech thành Quỹ tín dụng
                                hàng đầu Việt Nam.
                            </p>
                        </div>
                    </li>
                </ul>
            </section>
            <center class="my-4">
                <div class="linear-title">
                    NIỀM TIN ĐẾN TỪ NHỮNG CON SỐ
                </div>
            </center>
        </div>
        <section>
            <div class="tech-about__number"
                 style="background-image: url('{{ url('/images/about-number.png') }}');"></div>
        </section>
        <section class="tech-about__support">
            <div class="container">
                <div class="tech-about__support__inner">
                    <ul>
                        <li>
                            <div>
                                <img src="{{ url('/images/about-customer-sp.png') }}" height="48" width="50"/>
                                <div class="fw-bold text-uppercase ml-2">
                                    <p class="mb-0">Chăm sóc Khách hàng </p>
                                    <p class="mb-0">hoạt động 24/7</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <img src="{{ url('/images/about-hotline.png') }}" height="45" width="45"/>
                                <div class="fw-bold text-uppercase ml-2">
                                    <p class="mb-0">hotline</p>
                                    <p class="mb-0">1234.567.89</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <img src="{{ url('/images/about-chat.png') }}" height="48" width="53"/>
                                <div class="fw-bold text-uppercase ml-2">
                                    <p class="mb-0">chat tại đây</p>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('scripts')

@endsection