<!DOCTYPE html>
<html class="no-js" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-419" lang="en-419" dir="ltr"
      prefix="og: http://ogp.me/ns#">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta charset="utf-8"/>
    <link rel="icon" type="image/png" href="images/apple-logo.png" sizes="32x32">

    <link rel="canonical" href="https://www.apple.com/lae/apple-watch-series-4/"/>
    <link rel="alternate" href="https://www.apple.com/ae-ar/apple-watch-series-4/" hreflang="ar-AE"/>
    <link rel="alternate" href="https://www.apple.com/ae/apple-watch-series-4/" hreflang="en-AE"/>
    <link rel="alternate" href="https://www.apple.com/apple-watch-series-4/" hreflang="en-US"/>
    <link rel="alternate" href="https://www.apple.com/at/apple-watch-series-4/" hreflang="de-AT"/>
    <link rel="alternate" href="https://www.apple.com/au/apple-watch-series-4/" hreflang="en-AU"/>
    <link rel="alternate" href="https://www.apple.com/befr/apple-watch-series-4/" hreflang="fr-BE"/>
    <link rel="alternate" href="https://www.apple.com/benl/apple-watch-series-4/" hreflang="nl-BE"/>
    <link rel="alternate" href="https://www.apple.com/bh-ar/apple-watch-series-4/" hreflang="ar-BH"/>
    <link rel="alternate" href="https://www.apple.com/bh/apple-watch-series-4/" hreflang="en-BH"/>
    <link rel="alternate" href="https://www.apple.com/br/apple-watch-series-4/" hreflang="pt-BR"/>
    <link rel="alternate" href="https://www.apple.com/ca/apple-watch-series-4/" hreflang="en-CA"/>
    <link rel="alternate" href="https://www.apple.com/chde/apple-watch-series-4/" hreflang="de-CH"/>
    <link rel="alternate" href="https://www.apple.com/chfr/apple-watch-series-4/" hreflang="fr-CH"/>
    <link rel="alternate" href="https://www.apple.com/cl/apple-watch-series-4/" hreflang="es-CL"/>
    <link rel="alternate" href="https://www.apple.com/cn/apple-watch-series-4/" hreflang="zh-CN"/>
    <link rel="alternate" href="https://www.apple.com/co/apple-watch-series-4/" hreflang="es-CO"/>
    <link rel="alternate" href="https://www.apple.com/cz/apple-watch-series-4/" hreflang="cs-CZ"/>
    <link rel="alternate" href="https://www.apple.com/de/apple-watch-series-4/" hreflang="de-DE"/>
    <link rel="alternate" href="https://www.apple.com/dk/apple-watch-series-4/" hreflang="da-DK"/>
    <link rel="alternate" href="https://www.apple.com/es/apple-watch-series-4/" hreflang="es-ES"/>
    <link rel="alternate" href="https://www.apple.com/fi/apple-watch-series-4/" hreflang="fi-FI"/>
    <link rel="alternate" href="https://www.apple.com/fr/apple-watch-series-4/" hreflang="fr-FR"/>
    <link rel="alternate" href="https://www.apple.com/gr/apple-watch-series-4/" hreflang="el-GR"/>
    <link rel="alternate" href="https://www.apple.com/hk/apple-watch-series-4/" hreflang="zh-HK"/>
    <link rel="alternate" href="https://www.apple.com/hr/apple-watch-series-4/" hreflang="hr-HR"/>
    <link rel="alternate" href="https://www.apple.com/hu/apple-watch-series-4/" hreflang="hu-HU"/>
    <link rel="alternate" href="https://www.apple.com/id/apple-watch-series-4/" hreflang="en-ID"/>
    <link rel="alternate" href="https://www.apple.com/ie/apple-watch-series-4/" hreflang="en-IE"/>
    <link rel="alternate" href="https://www.apple.com/il/apple-watch-series-4/" hreflang="en-IL"/>
    <link rel="alternate" href="https://www.apple.com/in/apple-watch-series-4/" hreflang="en-IN"/>
    <link rel="alternate" href="https://www.apple.com/it/apple-watch-series-4/" hreflang="it-IT"/>
    <link rel="alternate" href="https://www.apple.com/jp/apple-watch-series-4/" hreflang="ja-JP"/>
    <link rel="alternate" href="https://www.apple.com/kr/apple-watch-series-4/" hreflang="kr-KR"/>
    <link rel="alternate" href="https://www.apple.com/kw-ar/apple-watch-series-4/" hreflang="ar-KW"/>
    <link rel="alternate" href="https://www.apple.com/kw/apple-watch-series-4/" hreflang="en-KW"/>
    <link rel="alternate" href="https://www.apple.com/la/apple-watch-series-4/" hreflang="es-EC"/>
    <link rel="alternate" href="https://www.apple.com/la/apple-watch-series-4/" hreflang="es-VE"/>
    <link rel="alternate" href="https://www.apple.com/la/apple-watch-series-4/" hreflang="es-PY"/>
    <link rel="alternate" href="https://www.apple.com/la/apple-watch-series-4/" hreflang="es-GT"/>
    <link rel="alternate" href="https://www.apple.com/la/apple-watch-series-4/" hreflang="es-DO"/>
    <link rel="alternate" href="https://www.apple.com/la/apple-watch-series-4/" hreflang="es-HN"/>
    <link rel="alternate" href="https://www.apple.com/la/apple-watch-series-4/" hreflang="es-BO"/>
    <link rel="alternate" href="https://www.apple.com/la/apple-watch-series-4/" hreflang="es-SV"/>
    <link rel="alternate" href="https://www.apple.com/la/apple-watch-series-4/" hreflang="es-AR"/>
    <link rel="alternate" href="https://www.apple.com/la/apple-watch-series-4/" hreflang="es-UY"/>
    <link rel="alternate" href="https://www.apple.com/la/apple-watch-series-4/" hreflang="es-NI"/>
    <link rel="alternate" href="https://www.apple.com/la/apple-watch-series-4/" hreflang="es-PA"/>
    <link rel="alternate" href="https://www.apple.com/la/apple-watch-series-4/" hreflang="es-PE"/>
    <link rel="alternate" href="https://www.apple.com/la/apple-watch-series-4/" hreflang="es-PR"/>
    <link rel="alternate" href="https://www.apple.com/la/apple-watch-series-4/" hreflang="es-CR"/>
    <link rel="alternate" href="https://www.apple.com/lae/apple-watch-series-4/" hreflang="en-DM"/>
    <link rel="alternate" href="https://www.apple.com/lae/apple-watch-series-4/" hreflang="en-KN"/>
    <link rel="alternate" href="https://www.apple.com/lae/apple-watch-series-4/" hreflang="en-LC"/>
    <link rel="alternate" href="https://www.apple.com/lae/apple-watch-series-4/" hreflang="en-KY"/>
    <link rel="alternate" href="https://www.apple.com/lae/apple-watch-series-4/" hreflang="en-AI"/>
    <link rel="alternate" href="https://www.apple.com/lae/apple-watch-series-4/" hreflang="en-GD"/>
    <link rel="alternate" href="https://www.apple.com/lae/apple-watch-series-4/" hreflang="en-MS"/>
    <link rel="alternate" href="https://www.apple.com/lae/apple-watch-series-4/" hreflang="en-TT"/>
    <link rel="alternate" href="https://www.apple.com/lae/apple-watch-series-4/" hreflang="en-VC"/>
    <link rel="alternate" href="https://www.apple.com/lae/apple-watch-series-4/" hreflang="en-SR"/>
    <link rel="alternate" href="https://www.apple.com/lae/apple-watch-series-4/" hreflang="en-PR"/>
    <link rel="alternate" href="https://www.apple.com/lae/apple-watch-series-4/" hreflang="en-BB"/>
    <link rel="alternate" href="https://www.apple.com/lae/apple-watch-series-4/" hreflang="en-BS"/>
    <link rel="alternate" href="https://www.apple.com/lae/apple-watch-series-4/" hreflang="en-JM"/>
    <link rel="alternate" href="https://www.apple.com/lae/apple-watch-series-4/" hreflang="en-AG"/>
    <link rel="alternate" href="https://www.apple.com/lae/apple-watch-series-4/" hreflang="en-TC"/>
    <link rel="alternate" href="https://www.apple.com/lae/apple-watch-series-4/" hreflang="en-BM"/>
    <link rel="alternate" href="https://www.apple.com/lae/apple-watch-series-4/" hreflang="en-BZ"/>
    <link rel="alternate" href="https://www.apple.com/lae/apple-watch-series-4/" hreflang="en-GY"/>
    <link rel="alternate" href="https://www.apple.com/lae/apple-watch-series-4/" hreflang="en-VG"/>
    <link rel="alternate" href="https://www.apple.com/lu/apple-watch-series-4/" hreflang="fr-LU"/>
    <link rel="alternate" href="https://www.apple.com/mo/apple-watch-series-4/" hreflang="zh-MO"/>
    <link rel="alternate" href="https://www.apple.com/mx/apple-watch-series-4/" hreflang="es-MX"/>
    <link rel="alternate" href="https://www.apple.com/my/apple-watch-series-4/" hreflang="en-MY"/>
    <link rel="alternate" href="https://www.apple.com/nl/apple-watch-series-4/" hreflang="nl-NL"/>
    <link rel="alternate" href="https://www.apple.com/no/apple-watch-series-4/" hreflang="no-NO"/>
    <link rel="alternate" href="https://www.apple.com/nz/apple-watch-series-4/" hreflang="en-NZ"/>
    <link rel="alternate" href="https://www.apple.com/om-ar/apple-watch-series-4/" hreflang="ar-OM"/>
    <link rel="alternate" href="https://www.apple.com/om/apple-watch-series-4/" hreflang="en-OM"/>
    <link rel="alternate" href="https://www.apple.com/ph/apple-watch-series-4/" hreflang="en-PH"/>
    <link rel="alternate" href="https://www.apple.com/pl/apple-watch-series-4/" hreflang="pl-PL"/>
    <link rel="alternate" href="https://www.apple.com/pt/apple-watch-series-4/" hreflang="pt-PT"/>
    <link rel="alternate" href="https://www.apple.com/qa-ar/apple-watch-series-4/" hreflang="ar-QA"/>
    <link rel="alternate" href="https://www.apple.com/qa/apple-watch-series-4/" hreflang="en-QA"/>
    <link rel="alternate" href="https://www.apple.com/ro/apple-watch-series-4/" hreflang="ro-RO"/>
    <link rel="alternate" href="https://www.apple.com/ru/apple-watch-series-4/" hreflang="ru-RU"/>
    <link rel="alternate" href="https://www.apple.com/sa-ar/apple-watch-series-4/" hreflang="ar-SA"/>
    <link rel="alternate" href="https://www.apple.com/sa/apple-watch-series-4/" hreflang="en-SA"/>
    <link rel="alternate" href="https://www.apple.com/se/apple-watch-series-4/" hreflang="sv-SE"/>
    <link rel="alternate" href="https://www.apple.com/sg/apple-watch-series-4/" hreflang="en-SG"/>
    <link rel="alternate" href="https://www.apple.com/sk/apple-watch-series-4/" hreflang="sk-SK"/>
    <link rel="alternate" href="https://www.apple.com/th/apple-watch-series-4/" hreflang="th-TH"/>
    <link rel="alternate" href="https://www.apple.com/tr/apple-watch-series-4/" hreflang="tr-TR"/>
    <link rel="alternate" href="https://www.apple.com/tw/apple-watch-series-4/" hreflang="zh-TW"/>
    <link rel="alternate" href="https://www.apple.com/uk/apple-watch-series-4/" hreflang="en-GB"/>
    <link rel="alternate" href="https://www.apple.com/vn/apple-watch-series-4/" hreflang="en-VN"/>
    <link rel="alternate" href="https://www.apple.com/za/apple-watch-series-4/" hreflang="en-ZA"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover"/>
    <link rel="stylesheet" type="text/css" href="ac/globalnav/4/en_419/styles/ac-globalnav.built.css"/>
    <link rel="stylesheet" type="text/css" href="ac/localnav/4/styles/ac-localnav.built.css"/>
    <link rel="stylesheet" type="text/css" href="ac/globalfooter/4/en_419/styles/ac-globalfooter.built.css"/>
    <!--  turn analyze console logs on dev mode-->
    <script type="text/javascript">
        console.analyze = function () {
        };
    </script>
    <title>Apple Watch Series 4 - Apple</title>
    <meta name="Description"
          content="Apple Watch Series 4 features its largest display yet, and a re-engineered digital crown."/>
    <meta property="og:image"
          content="https://www.apple.com/lae/apple-watch-series-4/images/overview/og__ccrx5vnr76uu.png?201903191613"/>
    <meta property="og:title" content="Apple Watch Series 4"/>
    <meta property="og:description"
          content="Apple Watch Series 4 features its largest display yet, and a re-engineered digital crown."/>
    <meta property="og:url" content="https://www.apple.com/lae/apple-watch-series-4/"/>
    <meta property="og:locale" content="en_419"/>
    <meta property="og:site_name" content="Apple (Latin America)"/>
    <meta property="og:type" content="website"/>
    <meta name="twitter:site" content="@Apple"/>
    <meta name="twitter:card" content="summary_large_image"/>
    <meta property="analytics-track" content="Apple Watch Series 4 - index"/>
    <meta property="analytics-s-channel" content="applewatch"/>
    <meta property="analytics-s-bucket-0" content="appleglobal,apple{COUNTRY_CODE}applewatch,applestoreww"/>
    <meta property="analytics-s-bucket-1"
          content="apple{COUNTRY_CODE}global,apple{COUNTRY_CODE}applewatch,applestoreww"/>
    <meta property="analytics-s-bucket-2" content="apple{COUNTRY_CODE}global,applestoreww"/>
    <link rel="stylesheet" href="wss/fonts_families_SF Pro_v2|SF Pro Icons_v1.css"/>
    <link rel="stylesheet" href="v/apple-watch-series-4/d/built/styles/main.built.css" type="text/css"/>
    <link rel="stylesheet" href="lae/apple-watch-series-4/styles/main.built.css" type="text/css"/>
    <link rel="stylesheet" href="v/apple-watch-series-4/d/built/styles/overview.built.css" type="text/css"/>
    <link rel="stylesheet" href="lae/apple-watch-series-4/styles/overview.built.css" type="text/css"/>
    <link rel="stylesheet" href="v/watch/compare-strip/h/built/styles/overview.built.css"/>
    <link rel="stylesheet" href="lae/watch/includes/compare-strip/styles/compare-strip.built.css" type="text/css"/>
    <link rel="stylesheet" href="https://www.apple.com/v/watch/compare/i/built/styles/overview.built.css"
          type="text/css"/>
    <script src="v/apple-watch-series-4/d/built/scripts/head.built.js" type="text/javascript" charset="utf-8"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body class="page-overview ac-gn-current-watch">
<meta name="ac-gn-store-key" content="SFX9YPYY9PPXCU9KH"/>
<input type="checkbox" id="ac-gn-menustate" class="ac-gn-menustate"/>
<nav id="ac-globalnav" class="no-js" role="navigation" aria-label="Global" data-hires="false"
     data-analytics-region="global nav" lang="en-419" dir="ltr" data-store-locale=""
     data-store-api="/[storefront]/shop/bag/status" data-search-locale="en_419"
     data-search-api="/search-services/suggestions/">
    <div class="ac-gn-content">
        <ul class="ac-gn-header">
            <li class="ac-gn-item ac-gn-menuicon">
                <label class="ac-gn-menuicon-label" for="ac-gn-menustate" aria-hidden="true">
					<span class="ac-gn-menuicon-bread ac-gn-menuicon-bread-top">
						<span class="ac-gn-menuicon-bread-crust ac-gn-menuicon-bread-crust-top"></span>
					</span>
                    <span class="ac-gn-menuicon-bread ac-gn-menuicon-bread-bottom">
						<span class="ac-gn-menuicon-bread-crust ac-gn-menuicon-bread-crust-bottom"></span>
					</span>
                </label>
                <a href="#ac-gn-menustate" role="button" class="ac-gn-menuanchor ac-gn-menuanchor-open"
                   id="ac-gn-menuanchor-open">
                    <span class="ac-gn-menuanchor-label">Global Nav Open Menu</span>
                </a>
                <a href="#" role="button" class="ac-gn-menuanchor ac-gn-menuanchor-close" id="ac-gn-menuanchor-close">
                    <span class="ac-gn-menuanchor-label">Global Nav Close Menu</span>
                </a>
            </li>
            <li class="ac-gn-item ac-gn-apple">
                <a class="ac-gn-link ac-gn-link-apple" href="lae.html" data-analytics-title="apple home"
                   id="ac-gn-firstfocus-small">
                    <span class="ac-gn-link-text">Apple</span>
                </a>
            </li>
        </ul>
        <div class="ac-gn-search-placeholder-container" role="search">
            <div class="ac-gn-search ac-gn-search-small">
                <a id="ac-gn-link-search-small" class="ac-gn-link" href="lae/search.html" data-analytics-title="search"
                   data-analytics-click="search" data-analytics-intrapage-link aria-label="Search apple.com">
                    <div class="ac-gn-search-placeholder-bar">
                        <div class="ac-gn-search-placeholder-input">
                            <div class="ac-gn-search-placeholder-input-text" aria-hidden="true">
                                <div class="ac-gn-link-search ac-gn-search-placeholder-input-icon"></div>
                                <span class="ac-gn-search-placeholder">Search apple.com</span>
                            </div>
                        </div>
                        <div class="ac-gn-searchview-close ac-gn-searchview-close-small ac-gn-search-placeholder-searchview-close">
                            <span class="ac-gn-searchview-close-cancel" aria-hidden="true">Cancel</span>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <ul class="ac-gn-list">
            <li class="ac-gn-item ac-gn-apple">
                <a class="ac-gn-link ac-gn-link-apple" href="lae.html" data-analytics-title="apple home"
                   id="ac-gn-firstfocus">
                    <span class="ac-gn-link-text">Apple</span>
                </a>
            </li>
            <li class="ac-gn-item ac-gn-item-menu ac-gn-mac">
                <a class="ac-gn-link ac-gn-link-mac" href="lae/mac.html" data-analytics-title="mac">
                    <span class="ac-gn-link-text">Mac</span>
                </a>
            </li>
            <li class="ac-gn-item ac-gn-item-menu ac-gn-ipad">
                <a class="ac-gn-link ac-gn-link-ipad" href="lae/ipad.html" data-analytics-title="ipad">
                    <span class="ac-gn-link-text">iPad</span>
                </a>
            </li>
            <li class="ac-gn-item ac-gn-item-menu ac-gn-iphone">
                <a class="ac-gn-link ac-gn-link-iphone" href="lae/iphone.html" data-analytics-title="iphone">
                    <span class="ac-gn-link-text">iPhone</span>
                </a>
            </li>
            <li class="ac-gn-item ac-gn-item-menu ac-gn-watch">
                <a class="ac-gn-link ac-gn-link-watch" href="lae/watch.html" data-analytics-title="watch">
                    <span class="ac-gn-link-text">Watch</span>
                </a>
            </li>
            <li class="ac-gn-item ac-gn-item-menu ac-gn-tv">
                <a class="ac-gn-link ac-gn-link-tv" href="lae/tv.html" data-analytics-title="tv">
                    <span class="ac-gn-link-text">TV</span>
                </a>
            </li>
            <li class="ac-gn-item ac-gn-item-menu ac-gn-music">
                <a class="ac-gn-link ac-gn-link-music" href="lae/music.html" data-analytics-title="music">
                    <span class="ac-gn-link-text">Music</span>
                </a>
            </li>
            <li class="ac-gn-item ac-gn-item-menu ac-gn-support">
                <a class="ac-gn-link ac-gn-link-support" href="support_subdomain/en-lamr.html"
                   data-analytics-title="support">
                    <span class="ac-gn-link-text">Support</span>
                </a>
            </li>
            <li class="ac-gn-item ac-gn-item-menu ac-gn-buy">
                <a class="ac-gn-link ac-gn-link-buy" href="#" data-analytics-title="buy">
                    <span class="ac-gn-link-text">Where to Buy</span>
                </a>
            </li>
            <li class="ac-gn-item ac-gn-item-menu ac-gn-search" role="search">
                <a id="ac-gn-link-search" class="ac-gn-link ac-gn-link-search" href="lae/search.html"
                   data-analytics-title="search" data-analytics-click="search" data-analytics-intrapage-link
                   aria-label="Search apple.com"></a>
            </li>
        </ul>
        <aside id="ac-gn-searchview" class="ac-gn-searchview" role="search" data-analytics-region="search">
            <div class="ac-gn-searchview-content">
                <div class="ac-gn-searchview-bar">
                    <div class="ac-gn-searchview-bar-wrapper">
                        <form id="ac-gn-searchform" class="ac-gn-searchform" action="lae/search.html" method="get">
                            <div class="ac-gn-searchform-wrapper">
                                <input id="ac-gn-searchform-input" class="ac-gn-searchform-input" type="text"
                                       aria-label="Search apple.com" placeholder="Search apple.com" autocorrect="off"
                                       autocapitalize="off" autocomplete="off" spellcheck="false" role="combobox"
                                       aria-autocomplete="list" aria-expanded="true"
                                       aria-owns="quicklinks suggestions"/>
                                <input id="ac-gn-searchform-src" type="hidden" name="src" value="globalnav"/>
                                <button id="ac-gn-searchform-submit" class="ac-gn-searchform-submit" type="submit"
                                        disabled aria-label="Submit Search"></button>
                                <button id="ac-gn-searchform-reset" class="ac-gn-searchform-reset" type="reset" disabled
                                        aria-label="Clear Search">
                                    <span class="ac-gn-searchform-reset-background"></span>
                                </button>
                            </div>
                        </form>
                        <button id="ac-gn-searchview-close-small"
                                class="ac-gn-searchview-close ac-gn-searchview-close-small" aria-label="Cancel Search">
								<span class="ac-gn-searchview-close-cancel" aria-hidden="true">
									Cancel
								</span>
                        </button>
                    </div>
                </div>
                <aside id="ac-gn-searchresults" class="ac-gn-searchresults" data-string-quicklinks="Quick Links"
                       data-string-suggestions="Suggested Searches" data-string-noresults=""></aside>
            </div>
            <button id="ac-gn-searchview-close" class="ac-gn-searchview-close" aria-label="Cancel Search">
					<span class="ac-gn-searchview-close-wrapper">
						<span class="ac-gn-searchview-close-left"></span>
						<span class="ac-gn-searchview-close-right"></span>
					</span>
            </button>
        </aside>
    </div>
</nav>
<div class="ac-gn-blur"></div>
<div id="ac-gn-curtain" class="ac-gn-curtain"></div>
<div id="ac-gn-placeholder" class="ac-nav-placeholder"></div>
<script type="text/javascript" src="ac/globalnav/4/en_419/scripts/ac-globalnav.built.js"></script>
<script src="metrics/ac-analytics/2-6-0/scripts/ac-analytics.js" type="text/javascript" charset="utf-8"></script>
<script src="metrics/ac-analytics/2-6-0/scripts/auto-init.js" type="text/javascript" charset="utf-8"></script>
<input type="checkbox" id="ac-ln-menustate" class="ac-ln-menustate"/>
<nav id="ac-localnav" class="no-js ac-localnav-custom ac-localnav-noborder ac-localnav-noblur" lang="en-419" dir="ltr"
     data-sticky data-analytics-region="local nav" role="navigation" aria-label="Local">
    <div class="ac-ln-wrapper">
        <div class="ac-ln-background"></div>
        <div class="ac-ln-content">
            <div class="ac-ln-title ac-ln-title-compact">
                <a href="#" data-analytics-title="product index">
                    Apple Watch Series 4</a>
            </div>
            <div class="ac-ln-menu">
                <a href="#ac-ln-menustate" class="ac-ln-menucta-anchor ac-ln-menucta-anchor-open"
                   id="ac-ln-menustate-open">
                    <span class="ac-ln-menucta-anchor-label">Open menu</span>
                </a>
                <a href="#" class="ac-ln-menucta-anchor ac-ln-menucta-anchor-close" id="ac-ln-menustate-close">
                    <span class="ac-ln-menucta-anchor-label">Close menu</span>
                </a>
                <div class="ac-ln-menu-tray">
                    <ul class="ac-ln-menu-items">
                        <li class="ac-ln-menu-item">
                            <span class="ac-ln-menu-link current" role="link" aria-disabled="true" aria-current="page"
                                  data-analytics-title="overview">Overview</span>
                        </li>
                        <li class="ac-ln-menu-item">
                            <a href="#" class="ac-ln-menu-link" data-analytics-title="design">Design</a>
                        </li>
                        <li class="ac-ln-menu-item">
                            <a href="#" class="ac-ln-menu-link" data-analytics-title="health">Health</a>
                        </li>
                        <li class="ac-ln-menu-item">
                            <a href="#" class="ac-ln-menu-link" data-analytics-title="workout">Workout</a>
                        </li>
                        <li class="ac-ln-menu-item">
                            <a href="#" class="ac-ln-menu-link" data-analytics-title="activity">Activity</a>
                        </li>
                        <li class="ac-ln-menu-item">
                            <a href="#" class="ac-ln-menu-link" data-analytics-title="connect">Connect</a>
                        </li>
                    </ul>
                </div>
                <div class="ac-ln-actions ac-ln-actions-center">
                    <div class="ac-ln-action ac-ln-action-menucta" aria-hidden="true">
                        <label for="ac-ln-menustate" class="ac-ln-menucta">
                            <span class="ac-ln-menucta-chevron"></span>
                        </label>
                    </div>
                    <div class="ac-ln-action ac-ln-action-button">
                        <a class="ac-ln-button" href="#">Buy<span class="ac-ln-action-product"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>
<label id="ac-ln-curtain" for="ac-ln-menustate"></label>
<script type="text/javascript" src="ac/localnav/4/scripts/ac-localnav.built.js"></script>
<main id="main" class="main" role="main" data-page-type="overview">
    <section class="section-overview-hero data-anim-time-group-hero" data-component-list="HeroComponent HeightComponent"
             data-height-container=".hero-image" data-height-margin="96" data-start="-100vh"
             data-analytics-section-engagement="name:hero">
        <div class="hero-image">
            <figure class="image-hero image-hero-startframe image-hero-start"></figure>
            <div class="image-hero-video-container">
                <video
                        class="video-landscape image-hero image-hero-video"
                        data-progressive-video
                        muted=""
                        playsinline="true"
                        data-video-source-basepath="/105/media/lae/apple-watch-series-4/2018/d33fb9a0-f7f4-4e2d-8e17-f29c2ac2460f/overview/hero/"
                        data-video-source-prefix="landscape_"
                        data-video-source-retina-enabled="false"
                        data-video-load-priority="10"
                ></video>
                <video
                        class="video-portrait image-hero image-hero-video"
                        data-progressive-video
                        muted=""
                        playsinline="true"
                        data-video-source-basepath="/105/media/lae/apple-watch-series-4/2018/d33fb9a0-f7f4-4e2d-8e17-f29c2ac2460f/overview/hero/"
                        data-video-source-prefix="portrait_"
                        data-video-source-retina-enabled="false"
                        data-video-load-priority="10"
                        data-video-prevent-load
                ></video>
            </div>
            <figure class="image-hero image-hero-fallback"></figure>
        </div>
        <style>
            html,
            body {
                height: 100%;
                /*overflow-x: hidden;*/
            }

            .row-form {
                font-family: "SF Pro Text","SF Pro Icons","Helvetica Neue","Helvetica","Arial",sans-serif;
                font-size: 17px;
                line-height: 1.47059;
                font-weight: 400;
                letter-spacing: -.022em;
                margin-top: 10px;
                color: gray;
            }

            @media only screen and (min-width: 998px) {
                .row-form {
                    text-align: right;
                }
                h4.typography-body {
                    text-align: right;
                }
            }
            .row-form input {
                border-radius: 10px;
                border: 2px solid gray;
                height: 25px;
            }

            .buy-form {
                padding-left: 20px;
                padding-right: 20px;
                margin-top: 40px;
            }
            select {
                width: 50%;
            }
            .colornav.colornav-focusring {
                width: 100%;
            }
            h4.typography-body {
                padding-top: 10px;
            }

            .colornav.colornav-focusring {
                margin-top: 0px !important;
                margin-bottom: 5px;
            }
            div#check-code {
                text-align: center;
                line-height: 15px;
            }
            .modal-backdrop {
                z-index: -1;
            }
            p.amount-product {
                text-align: center;
                font-size: 25px;
                margin-top: 15px;
            }
            span.number-product {color: #f42329;font-weight: bold;}
        </style>
        <p id="order-product" class="amount-product">Chỉ còn <span class="number-product">{{$amount_product}}</span> sản phẩm khuyến mãi</p>
        <div class="buy-form">
            <div class="row" style="height: 100%;
                display: flex;
                justify-content: center;
                align-items: center;">
                <form method="put" action="{{route('order')}}" class="col-md-6 col-xs-12">
                    <input id="randomstring" name="code" hidden>
                    <div class="row row-form" style="
    text-align: left;
    font-size: 20px;
">
                        <p class="customer-info">Thông tin khách hàng: </p>
                    </div>
                    <div class="row row-form">
                        <span for="email" class="col-md-6 col-xs-12 label-form">Họ tên:</span>
                        <input type="text" class="col-md-6 col-xs-12 " placeholder="Nhập họ tên" name="name" required="">
                    </div>

                    <div class="row row-form">
                        <span for="psw" class="col-md-6 col-xs-12 label-form">Địa chỉ: </span>
                        <input type="text" class="col-md-6 col-xs-12" placeholder="Nhập địa chỉ" name="address" required="">
                    </div>

                    <div class="row row-form">
                        <span for="psw-repeat" class="col-md-6 col-xs-12 label-form">Số điện thoại giao hàng: </span>
                        <input type="text" class="col-md-6 col-xs-12" placeholder="Số điện thoại" name="phone" required="">
                    </div>
                    <hr>
                    <div class="row row-form" style="
    text-align: left;
    font-size: 20px;
">
                        <p class="customer-info">Thông tin sản phẩm: </p>
                    </div>
                    <div class="row row-form">
                        <span for="psw-repeat" class="col-md-6 col-xs-12 label-form">Model: </span>
                        <select name="model" id="model" onchange="ChangeModel()">
                            <option value="GPS + Cellular">GPS + Cellular</option>
                            <option value="GPS">GPS</option>
                        </select>
                    </div>
                    <div class="row row-form">
                        <span for="psw-repeat" class="col-md-6 col-xs-12 label-form">Chất liệu:</span>
                        <input type="text" class="col-md-6 col-xs-12" placeholder="Số điện thoại" value="Aluminum" disabled="true">
                    </div>

                    <div class="row row-form">
                        <span for="psw-repeat" class="col-md-6 col-xs-12 label-form">Mã giảm giá: </span>
                        <input type="text" id="coupon" class="col-md-4 col-xs-8" placeholder="Mã giảm giá" >
                        <div class="col-md-2 col-xs-4 btn btn-default" onclick="checkCode()" data-toggle="modal" data-target="#myModal" id="check-code">Kiểm tra</div>
                        <input name="coupon" id="coupon-value" hidden>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Kiểm tra mã giảm giá</h4>
                                </div>
                                <div class="modal-body" id="modal-body" style="text-align: center">

                                </div>
                                <div class="modal-footer" id="modal-footer">

                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row row-form">
                        <div class="page-overview">
                            <div class="section-colors">
                                <div class="colornav colornav-focusring">
                                    <div class="col-md-6 col-xs-12">
                                        <h4 class="typography-body">Màu sắc:</h4>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <ul onclick="selectColor(1)" class="colornav-items" role="tablist">
                                            <li class="colornav-item" role="presentation">
                                                <div id="series-4-gps-cellular-finish1-option1-trigger" class="colornav-link current" data-ac-gallery-trigger="series-4-gps-cellular-finish1-option1" role="tab" tabindex="0" aria-controls="series-4-gps-cellular-finish1-option1" aria-selected="true">
                                                    <figure class="colornav-swatch colornav-swatch-silver">
                                                        <figcaption class="colornav-label">Silver</figcaption>
                                                    </figure>
                                                </div>
                                            </li>
                                        </ul>
                                        <ul onclick="selectColor(2)" class="colornav-items" role="tablist">
                                            <li class="colornav-item" role="presentation">
                                                <div id="series-4-gps-cellular-finish1-option2-trigger" class="colornav-link" data-ac-gallery-trigger="series-4-gps-cellular-finish1-option2" role="tab" tabindex="0" aria-controls="series-4-gps-cellular-finish1-option2" aria-selected="false">
                                                    <figure class="colornav-swatch colornav-swatch-space-black">
                                                        <figcaption class="colornav-label">Space Black<br>(DLC)</figcaption>
                                                    </figure>
                                                </div>
                                            </li>
                                        </ul>
                                        <ul onclick="selectColor(3)" class="colornav-items" role="tablist">
                                            <li class="colornav-item" role="presentation">
                                                <div id="series-4-gps-cellular-finish1-option3-trigger" class="colornav-link" data-ac-gallery-trigger="series-4-gps-cellular-finish1-option3" role="tab" tabindex="0" aria-controls="series-4-gps-cellular-finish1-option3" aria-selected="false">
                                                    <figure class="colornav-swatch colornav-swatch-ss-gold">
                                                        <figcaption class="colornav-label">Gold<br>(PVD)</figcaption>
                                                    </figure>
                                                </div>
                                            </li>
                                        </ul>
                                        <input name="color" id="color" value="silver" hidden>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row row-form">
                        <span for="psw-repeat" class="col-md-6 col-xs-6 label-form">Giá sản phẩm : </span>
                        <span id="price-model" >11.900.000 VNĐ</span>
                    </div>
                    <div class="row row-form">
                        <span for="psw-repeat" class="col-md-6 col-xs-6 label-form">Khuyến mãi : </span>
                        <span id="discount-percent" ></span>
                        <span id="price-show" class="money">0 VNĐ</span>
                        <input id="pre-price" value="11900000" hidden>
                        <input name="price" id="price" value="11900000" hidden>
                    </div>
                    <div class="row row-form">
                        <span for="psw-repeat" class="col-md-6 col-xs-6 label-form">Phí vận chuyển : </span>
                        <span >80.000 VNĐ</span>
                    </div>
                    <div class="row row-form">
                        <span for="psw-repeat" class="col-md-6 col-xs-6 label-form">Phí bảo hành : </span>
                        <span >Miễn phí (1 năm)</span>
                    </div>
                    <div class="row row-form">
                        <span for="psw-repeat" class="col-md-6 col-xs-6 label-form">Tổng cộng : </span>
                        <span id="total-price">11.980.000 VNĐ</span>
                    </div>
                    <div class="row row-form" style="text-align: center; margin-bottom: 20px;">
                        <button type="submit" class="registerbtn" style="
                                background-color: #f42329;
                                width: 40%;
                                height: 40px;
                                border-radius: 25px;
                                color: white;
                                text-align: center;
                                border: 1px solid gray;">
                            Mua ngay
                        </button>
                    </div>
                </form>
            </div>


        </div>
        <div class="hero-copy">
            <div class="section-intro"
                 data-anim-classname='{"relativeTo":".section-overview-hero","start":"-100vh","end":"-50vh","cssClass":"will-change","toggle":true,"disabledWhen":["height-fallback","static-fallback"]}'
                 data-anim-tween='{"start":"-70vh","end":"-70vh + 60px","opacity":[0,1],"ease":0,"easeFunction":"linear","disabledWhen":["height-fallback","static-fallback"]}'>
                <div class="hero-logo">
                    <figure class="image-logo"></figure>
                </div>
                <h1 class="hero-copy-headline typography-headline-super" data-component-list="EnhancementComponent">
                    Change starts within.</h1>
                <p class="hero-copy-paragraph typography-manifesto">Apple Watch Series 4. Fundamentally redesigned and
                    re&#8209;engineered to help you be even more active, healthy, and connected.</p>
            </div>
        </div>
    </section>
    <section class="section-overview section-design" data-anim-scroll-group="section-scroll-design"
             data-component-list="VideoSection ExperienceTracker" data-start="-25vh" data-reverse="0% - 100vh"
             data-analytics-section-engagement="name:design">
        <div class="section-borders" data-component-list="BorderComponent" data-name="design" data-reverse="false"
             data-anim-scroll-group="border-group-design">
            <div class="section-background">
                <div class="hero-video-group"
                     data-anim-classname='{"relativeTo":".section-design","start":"-10vh - 96px","end":"100rh","cssClass":"will-change","toggle":true,"disabledWhen":["height-fallback","static-fallback"]}'
                     data-anim-tween='{"relativeTo":".section-design","start":"100rh - 100vh","end":"100rh","y":[0,"25vh"],"ease":0.6,"easeFunction":"linear","disabledWhen":["height-fallback","static-fallback"],"breakpointMask":"MLX"}'>
                    <figure class="image-hero-static image-hero-static-design"
                            data-component-list="ProgressiveImageLoaderComponent" data-progressive-image></figure>
                    <figure class="image-hero-start image-hero-start-design"
                            data-component-list="ProgressiveImageLoaderComponent" data-progressive-image></figure>
                    <div class="image-hero-video-container">
                        <video
                                class="video-landscape"
                                data-progressive-video
                                muted=""
                                playsinline="true"
                                data-video-source-basepath="/105/media/ww/apple-watch-series-4/2018/d33fb9a0-f7f4-4e2d-8e17-f29c2ac2460f/section_heros/design/"
                                data-video-source-prefix="landscape_"
                                data-video-source-retina-enabled="false"
                                data-video-load-priority="1"
                        ></video>
                        <video
                                class="video-portrait"
                                data-progressive-video
                                muted=""
                                playsinline="true"
                                data-video-source-basepath="/105/media/ww/apple-watch-series-4/2018/d33fb9a0-f7f4-4e2d-8e17-f29c2ac2460f/section_heros/design/"
                                data-video-source-prefix="portrait_"
                                data-video-source-retina-enabled="false"
                                data-video-load-priority="1"
                                data-video-prevent-load
                                tabindex="-1"
                        ></video>
                    </div>
                    <div class="section-background-overlay"
                         data-anim-classname='{"relativeTo":".section-design","start":"-5vh","end":"100rh","cssClass":"will-change","toggle":true,"disabledWhen":["height-fallback","static-fallback"]}'
                         data-anim-tween-1='{"relativeTo":".section-design","start":"0","end":"176px","opacity":[0,1],"easeFunction":"linear","disabledWhen":["height-fallback","static-fallback"],"breakpointMask":"L"}'
                         data-anim-tween-2='{"relativeTo":".section-design","start":"0","end":"132px","opacity":[0,1],"easeFunction":"linear","disabledWhen":["height-fallback","static-fallback"],"breakpointMask":"M"}'
                         data-anim-tween-3='{"relativeTo":".section-design","start":"0","end":"80px","opacity":[0,1],"easeFunction":"linear","disabledWhen":["height-fallback","static-fallback"],"breakpointMask":"S"}'>
                    </div>
                </div>
            </div>
            <div class="section-borders-inside">
                <div class="section-border section-border-right" data-scale="x">
                </div>
                <div class="section-border section-border-left" data-scale="x">
                </div>
                <div class="section-border section-border-top" data-scale="top"></div>
                <div class="section-border section-border-bottom" data-scale="bottom"></div>
            </div>
        </div>
        <div class="section-content" data-component-list="AnimatedCopyComponent" data-name="design" data-start="-75vh">
            <div class="section-content-inside section-content-inside-design">
                <div class="section-content-copy">
                    <h2 class="c-1">
                        <span class="overview-section-eyebrow typography-quote-reduced copy-element">All-New Design</span>
                        <span class="overview-section-headline typography-headline-super copy-element">Not just evolved, transformed.</span>
                    </h2>
                </div>
                <div class="section-content-copy">
                    <div class="c-1 copy-element">
                        <p class="overview-section-copy typography-intro-elevated">The largest Apple Watch display yet.
                            Electrical heart sensor. Re-engineered Digital Crown with haptic feedback. Entirely
                            familiar, yet completely redesigned, Apple Watch Series 4 resets the standard for what a
                            watch can be.</p>
                        <a class="cta-button" href="#" data-analytics-region="learn more"
                           data-analytics-title="learn more about design" aria-label="All-New Design. Learn more.">
                            <span class="cta">Learn more</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="subpage-video-controls">
            <div class="video-controls-container" role="button" aria-label="Pause/Play Video">
                <div class="loader">
                    <div class="circle-container circle"></div>
                    <button class="pause-icon" data-analytics-click="prop3:pause-design"
                            data-analytics-title="pause-design"></button>
                    <button class="play-icon" data-analytics-click="prop3:play-design"
                            data-analytics-title="play-design"></button>
                </div>
            </div>
        </div>
    </section>
    <section class="section-overview section-health" data-anim-scroll-group="section-scroll-health"
             data-component-list="VideoSection ExperienceTracker" data-start="-25vh" data-reverse="0% - 100vh"
             data-analytics-section-engagement="name:health">
        <div class="section-borders" data-component-list="BorderComponent" data-name="health" data-reverse="false"
             data-anim-scroll-group="border-group-health">
            <div class="section-background">
                <div class="hero-video-group"
                     data-anim-classname='{"relativeTo":".section-health","start":"-10vh - 96px","end":"100rh","cssClass":"will-change","toggle":true,"disabledWhen":["height-fallback","static-fallback"]}'
                     data-anim-tween='{"relativeTo":".section-health","start":"100rh - 100vh","end":"100rh","y":[0,"25vh"],"ease":0.6,"easeFunction":"linear","disabledWhen":["height-fallback","static-fallback"],"breakpointMask":"MLX"}'>
                    <figure class="image-hero-static image-hero-static-health"
                            data-component-list="ProgressiveImageLoaderComponent" data-progressive-image></figure>
                    <figure class="image-hero-start image-hero-start-health"
                            data-component-list="ProgressiveImageLoaderComponent" data-progressive-image></figure>
                    <div class="image-hero-video-container">
                        <video
                                class="video-landscape"
                                data-progressive-video
                                muted=""
                                playsinline="true"
                                data-video-source-basepath="/105/media/ww/apple-watch-series-4/2018/d33fb9a0-f7f4-4e2d-8e17-f29c2ac2460f/section_heros/health/"
                                data-video-source-prefix="landscape_"
                                data-video-source-retina-enabled="false"
                                data-video-load-priority="1"
                        ></video>
                        <video
                                class="video-portrait"
                                data-progressive-video
                                muted=""
                                playsinline="true"
                                data-video-source-basepath="/105/media/ww/apple-watch-series-4/2018/d33fb9a0-f7f4-4e2d-8e17-f29c2ac2460f/section_heros/health/"
                                data-video-source-prefix="portrait_"
                                data-video-source-retina-enabled="false"
                                data-video-load-priority="1"
                                data-video-prevent-load
                                tabindex="-1"
                        ></video>
                    </div>
                    <div class="section-background-overlay"
                         data-anim-classname='{"relativeTo":".section-health","start":"-5vh","end":"100rh","cssClass":"will-change","toggle":true,"disabledWhen":["height-fallback","static-fallback"]}'
                         data-anim-tween-1='{"relativeTo":".section-health","start":"0","end":"176px","opacity":[0,1],"easeFunction":"linear","disabledWhen":["height-fallback","static-fallback"],"breakpointMask":"L"}'
                         data-anim-tween-2='{"relativeTo":".section-health","start":"0","end":"132px","opacity":[0,1],"easeFunction":"linear","disabledWhen":["height-fallback","static-fallback"],"breakpointMask":"M"}'
                         data-anim-tween-3='{"relativeTo":".section-health","start":"0","end":"80px","opacity":[0,1],"easeFunction":"linear","disabledWhen":["height-fallback","static-fallback"],"breakpointMask":"S"}'>
                    </div>
                </div>
            </div>
            <div class="section-borders-inside">
                <div class="section-border section-border-right" data-scale="x">
                </div>
                <div class="section-border section-border-left" data-scale="x">
                </div>
                <div class="section-border section-border-top" data-scale="top"></div>
                <div class="section-border section-border-bottom" data-scale="bottom"></div>
            </div>
        </div>
        <div class="section-content" data-component-list="AnimatedCopyComponent" data-name="health" data-start="-75vh">
            <div class="section-content-inside section-content-inside-health">
                <div class="section-content-copy">
                    <h2 class="c-1">
                        <span class="overview-section-eyebrow typography-quote-reduced copy-element">Proactive Health Monitor</span>
                        <span class="overview-section-headline typography-headline-super copy-element">Part guardian. Part guru.</span>
                    </h2>
                </div>
                <div class="section-content-copy">
                    <div class="c-1 copy-element">
                        <p class="overview-section-copy typography-intro-elevated">Notifications for low and high heart
                            rate. Fall detection and Emergency SOS. Breathe watch faces. It’s designed to improve your
                            health every day and powerful enough to help protect it.</p>
                        <a class="cta-button" href="#" data-analytics-region="learn more"
                           data-analytics-title="learn more about health"
                           aria-label="Proactive Health Monitor. Learn more.">
                            <span class="cta">Learn more</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="subpage-video-controls">
            <div class="video-controls-container" role="button" aria-label="Pause/Play Video">
                <div class="loader">
                    <div class="circle-container circle"></div>
                    <button class="pause-icon" data-analytics-click="prop3:pause-health"
                            data-analytics-title="pause-health"></button>
                    <button class="play-icon" data-analytics-click="prop3:play-health"
                            data-analytics-title="play-health"></button>
                </div>
            </div>
        </div>
    </section>
    <section class="section-overview section-workout" data-anim-scroll-group="section-scroll-workout"
             data-component-list="VideoSection ExperienceTracker" data-start="-25vh" data-reverse="0% - 100vh"
             data-analytics-section-engagement="name:workout">
        <div class="section-borders" data-component-list="BorderComponent" data-name="workout" data-reverse="false"
             data-anim-scroll-group="border-group-workout">
            <div class="section-background">
                <div class="hero-video-group"
                     data-anim-classname='{"relativeTo":".section-workout","start":"-10vh - 96px","end":"100rh","cssClass":"will-change","toggle":true,"disabledWhen":["height-fallback","static-fallback"]}'
                     data-anim-tween='{"relativeTo":".section-workout","start":"100rh - 100vh","end":"100rh","y":[0,"25vh"],"ease":0.6,"easeFunction":"linear","disabledWhen":["height-fallback","static-fallback"],"breakpointMask":"MLX"}'>
                    <figure class="image-hero-static image-hero-static-workout"
                            data-component-list="ProgressiveImageLoaderComponent" data-progressive-image></figure>
                    <figure class="image-hero-start image-hero-start-workout"
                            data-component-list="ProgressiveImageLoaderComponent" data-progressive-image></figure>
                    <div class="image-hero-video-container">
                        <video
                                class="video-landscape"
                                data-progressive-video
                                muted=""
                                playsinline="true"
                                data-video-source-basepath="/105/media/ww/apple-watch-series-4/2018/d33fb9a0-f7f4-4e2d-8e17-f29c2ac2460f/section_heros/workout/"
                                data-video-source-prefix="landscape_"
                                data-video-source-retina-enabled="false"
                                data-video-load-priority="1"
                        ></video>
                        <video
                                class="video-portrait"
                                data-progressive-video
                                muted=""
                                playsinline="true"
                                data-video-source-basepath="/105/media/ww/apple-watch-series-4/2018/d33fb9a0-f7f4-4e2d-8e17-f29c2ac2460f/section_heros/workout/"
                                data-video-source-prefix="portrait_"
                                data-video-source-retina-enabled="false"
                                data-video-load-priority="1"
                                data-video-prevent-load
                                tabindex="-1"
                        ></video>
                    </div>
                    <div class="section-background-overlay"
                         data-anim-classname='{"relativeTo":".section-workout","start":"-5vh","end":"100rh","cssClass":"will-change","toggle":true,"disabledWhen":["height-fallback","static-fallback"]}'
                         data-anim-tween-1='{"relativeTo":".section-workout","start":"0","end":"176px","opacity":[0,1],"easeFunction":"linear","disabledWhen":["height-fallback","static-fallback"],"breakpointMask":"L"}'
                         data-anim-tween-2='{"relativeTo":".section-workout","start":"0","end":"132px","opacity":[0,1],"easeFunction":"linear","disabledWhen":["height-fallback","static-fallback"],"breakpointMask":"M"}'
                         data-anim-tween-3='{"relativeTo":".section-workout","start":"0","end":"80px","opacity":[0,1],"easeFunction":"linear","disabledWhen":["height-fallback","static-fallback"],"breakpointMask":"S"}'>
                    </div>
                </div>
            </div>
            <div class="section-borders-inside">
                <div class="section-border section-border-right" data-scale="x">
                </div>
                <div class="section-border section-border-left" data-scale="x">
                </div>
                <div class="section-border section-border-top" data-scale="top"></div>
                <div class="section-border section-border-bottom" data-scale="bottom"></div>
            </div>
        </div>
        <div class="section-content" data-component-list="AnimatedCopyComponent" data-name="workout" data-start="-75vh">
            <div class="section-content-inside section-content-inside-workout">
                <div class="section-content-copy">
                    <h2 class="c-1">
                        <span class="overview-section-eyebrow typography-quote-reduced copy-element">Ultimate Workout Partner</span>
                        <span class="overview-section-headline typography-headline-super copy-element">Workouts that work harder.</span>
                    </h2>
                </div>
                <div class="section-content-copy">
                    <div class="c-1 copy-element">
                        <p class="overview-section-copy typography-intro-elevated">Automatic workout detection. Yoga and
                            hiking workouts. Advanced features for runners like cadence and pace alerts. See up to five
                            metrics at a glance as you precisely track all your favorite ways to train.</p>
                        <a class="cta-button" href="#" data-analytics-region="learn more"
                           data-analytics-title="learn more about workout"
                           aria-label="Ultimate Workout Partner. Learn more.">
                            <span class="cta">Learn more</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="subpage-video-controls">
            <div class="video-controls-container" role="button" aria-label="Pause/Play Video">
                <div class="loader">
                    <div class="circle-container circle"></div>
                    <button class="pause-icon" data-analytics-click="prop3:pause-workout"
                            data-analytics-title="pause-workout"></button>
                    <button class="play-icon" data-analytics-click="prop3:play-workout"
                            data-analytics-title="play-workout"></button>
                </div>
            </div>
        </div>
    </section>
    <section class="section-overview section-activity" data-anim-scroll-group="section-scroll-activity"
             data-component-list="VideoSection ExperienceTracker" data-start="-25vh" data-reverse="0% - 100vh"
             data-analytics-section-engagement="name:activity">
        <div class="section-borders" data-component-list="BorderComponent" data-name="activity" data-reverse="false"
             data-anim-scroll-group="border-group-activity">
            <div class="section-background">
                <div class="hero-video-group"
                     data-anim-classname='{"relativeTo":".section-activity","start":"-10vh - 96px","end":"100rh","cssClass":"will-change","toggle":true,"disabledWhen":["height-fallback","static-fallback"]}'
                     data-anim-tween='{"relativeTo":".section-activity","start":"100rh - 100vh","end":"100rh","y":[0,"25vh"],"ease":0.6,"easeFunction":"linear","disabledWhen":["height-fallback","static-fallback"],"breakpointMask":"MLX"}'>
                    <figure class="image-hero-static image-hero-static-activity"
                            data-component-list="ProgressiveImageLoaderComponent" data-progressive-image></figure>
                    <figure class="image-hero-start image-hero-start-activity"
                            data-component-list="ProgressiveImageLoaderComponent" data-progressive-image></figure>
                    <div class="image-hero-video-container">
                        <video
                                class="video-landscape"
                                data-progressive-video
                                muted=""
                                playsinline="true"
                                data-video-source-basepath="/105/media/ww/apple-watch-series-4/2018/d33fb9a0-f7f4-4e2d-8e17-f29c2ac2460f/section_heros/activity/"
                                data-video-source-prefix="landscape_"
                                data-video-source-retina-enabled="false"
                                data-video-load-priority="1"
                        ></video>
                        <video
                                class="video-portrait"
                                data-progressive-video
                                muted=""
                                playsinline="true"
                                data-video-source-basepath="/105/media/ww/apple-watch-series-4/2018/d33fb9a0-f7f4-4e2d-8e17-f29c2ac2460f/section_heros/activity/"
                                data-video-source-prefix="portrait_"
                                data-video-source-retina-enabled="false"
                                data-video-load-priority="1"
                                data-video-prevent-load
                                tabindex="-1"
                        ></video>
                    </div>
                    <div class="section-background-overlay"
                         data-anim-classname='{"relativeTo":".section-activity","start":"-5vh","end":"100rh","cssClass":"will-change","toggle":true,"disabledWhen":["height-fallback","static-fallback"]}'
                         data-anim-tween-1='{"relativeTo":".section-activity","start":"0","end":"176px","opacity":[0,1],"easeFunction":"linear","disabledWhen":["height-fallback","static-fallback"],"breakpointMask":"L"}'
                         data-anim-tween-2='{"relativeTo":".section-activity","start":"0","end":"132px","opacity":[0,1],"easeFunction":"linear","disabledWhen":["height-fallback","static-fallback"],"breakpointMask":"M"}'
                         data-anim-tween-3='{"relativeTo":".section-activity","start":"0","end":"80px","opacity":[0,1],"easeFunction":"linear","disabledWhen":["height-fallback","static-fallback"],"breakpointMask":"S"}'>
                    </div>
                </div>
            </div>
            <div class="section-borders-inside">
                <div class="section-border section-border-right" data-scale="x">
                </div>
                <div class="section-border section-border-left" data-scale="x">
                </div>
                <div class="section-border section-border-top" data-scale="top"></div>
                <div class="section-border section-border-bottom" data-scale="bottom"></div>
            </div>
        </div>
        <div class="section-content" data-component-list="AnimatedCopyComponent" data-name="activity"
             data-start="-75vh">
            <div class="section-content-inside section-content-inside-activity">
                <div class="section-content-copy">
                    <h2 class="c-1">
                        <span class="overview-section-eyebrow typography-quote-reduced copy-element">Comprehensive Activity Tracker</span>
                        <span class="overview-section-headline typography-headline-super copy-element">Made to motivate.</span>
                    </h2>
                </div>
                <div class="section-content-copy">
                    <div class="c-1 copy-element">
                        <p class="overview-section-copy typography-intro-elevated">Head-to-head competitions. Activity
                            sharing with friends. Personalized coaching. Monthly challenges. Achievement awards. Get all
                            the motivation you need to close your Activity rings every day.</p>
                        <a class="cta-button" href="#" data-analytics-region="learn more"
                           data-analytics-title="learn more about activity"
                           aria-label="Comprehensive Activity Tracker. Learn more.">
                            <span class="cta">Learn more</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="subpage-video-controls">
            <div class="video-controls-container" role="button" aria-label="Pause/Play Video">
                <div class="loader">
                    <div class="circle-container circle"></div>
                    <button class="pause-icon" data-analytics-click="prop3:pause-activity"
                            data-analytics-title="pause-activity"></button>
                    <button class="play-icon" data-analytics-click="prop3:play-activity"
                            data-analytics-title="play-activity"></button>
                </div>
            </div>
        </div>
    </section>
    <section class="section-overview section-connect" data-anim-scroll-group="section-scroll-connect"
             data-component-list="VideoSection ExperienceTracker" data-start="-25vh" data-reverse="0% - 100vh"
             data-analytics-section-engagement="name:connect">
        <div class="section-borders" data-component-list="BorderComponent" data-name="connect" data-reverse="false"
             data-anim-scroll-group="border-group-connect">
            <div class="section-background">
                <div class="hero-video-group"
                     data-anim-classname='{"relativeTo":".section-connect","start":"-10vh - 96px","end":"100rh","cssClass":"will-change","toggle":true,"disabledWhen":["height-fallback","static-fallback"]}'
                     data-anim-tween='{"relativeTo":".section-connect","start":"100rh - 100vh","end":"100rh","y":[0,"25vh"],"ease":0.6,"easeFunction":"linear","disabledWhen":["height-fallback","static-fallback"],"breakpointMask":"MLX"}'>
                    <figure class="image-hero-static image-hero-static-connect"
                            data-component-list="ProgressiveImageLoaderComponent" data-progressive-image></figure>
                    <figure class="image-hero-start image-hero-start-connect"
                            data-component-list="ProgressiveImageLoaderComponent" data-progressive-image></figure>
                    <div class="image-hero-video-container">
                        <video
                                class="video-landscape"
                                data-progressive-video
                                muted=""
                                playsinline="true"
                                data-video-source-basepath="/105/media/ww/apple-watch-series-4/2018/d33fb9a0-f7f4-4e2d-8e17-f29c2ac2460f/section_heros/connect/"
                                data-video-source-prefix="landscape_"
                                data-video-source-retina-enabled="false"
                                data-video-load-priority="1"
                        ></video>
                        <video
                                class="video-portrait"
                                data-progressive-video
                                muted=""
                                playsinline="true"
                                data-video-source-basepath="/105/media/ww/apple-watch-series-4/2018/d33fb9a0-f7f4-4e2d-8e17-f29c2ac2460f/section_heros/connect/"
                                data-video-source-prefix="portrait_"
                                data-video-source-retina-enabled="false"
                                data-video-load-priority="1"
                                data-video-prevent-load
                                tabindex="-1"
                        ></video>
                    </div>
                    <div class="section-background-overlay"
                         data-anim-classname='{"relativeTo":".section-connect","start":"-5vh","end":"100rh","cssClass":"will-change","toggle":true,"disabledWhen":["height-fallback","static-fallback"]}'
                         data-anim-tween-1='{"relativeTo":".section-connect","start":"0","end":"176px","opacity":[0,1],"easeFunction":"linear","disabledWhen":["height-fallback","static-fallback"],"breakpointMask":"L"}'
                         data-anim-tween-2='{"relativeTo":".section-connect","start":"0","end":"132px","opacity":[0,1],"easeFunction":"linear","disabledWhen":["height-fallback","static-fallback"],"breakpointMask":"M"}'
                         data-anim-tween-3='{"relativeTo":".section-connect","start":"0","end":"80px","opacity":[0,1],"easeFunction":"linear","disabledWhen":["height-fallback","static-fallback"],"breakpointMask":"S"}'>
                    </div>
                </div>
            </div>
            <div class="section-borders-inside">
                <div class="section-border section-border-right" data-scale="x">
                </div>
                <div class="section-border section-border-left" data-scale="x">
                </div>
                <div class="section-border section-border-top" data-scale="top"></div>
                <div class="section-border section-border-bottom" data-scale="bottom"></div>
            </div>
        </div>
        <div class="section-content" data-component-list="AnimatedCopyComponent" data-name="connect" data-start="-75vh">
            <div class="section-content-inside section-content-inside-connect">
                <div class="section-content-copy">
                    <h2 class="c-1">
                        <span class="overview-section-eyebrow typography-quote-reduced copy-element">Easy Ways to Connect</span>
                        <span class="overview-section-headline typography-headline-super copy-element">Stay close to what <br
                                    class="large-hide medium-show small-hide">you care about.</span>
                    </h2>
                </div>
                <div class="section-content-copy">
                    <div class="c-1 copy-element">
                        <p class="overview-section-copy typography-intro-elevated">Walkie-Talkie, phone calls, and
                            messages. Apple Music and Apple Podcasts.<a href="#footnote-1" class="footnote">*</a>
                            They’re all there, right on your wrist.</p>
                        <a class="cta-button" href="#" data-analytics-region="learn more"
                           data-analytics-title="learn more about how to connect"
                           aria-label="Easy Ways to Connect. Learn more.">
                            <span class="cta">Learn more</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="subpage-video-controls">
            <div class="video-controls-container" role="button" aria-label="Pause/Play Video">
                <div class="loader">
                    <div class="circle-container circle"></div>
                    <button class="pause-icon" data-analytics-click="prop3:pause-connect"
                            data-analytics-title="pause-connect"></button>
                    <button class="play-icon" data-analytics-click="prop3:play-connect"
                            data-analytics-title="play-connect"></button>
                </div>
            </div>
        </div>
    </section>
    <section class="section-compare" data-analytics-section-engagement="name:compare">
        <div class="section-wrapper">
            <h2 class="typography-headline">There’s an Apple Watch for everyone.</h2>

            <ul class="section-compare-grid" role="list">
                <li class="compare-item-series-4">
                    <figure class="compare-item-image" data-component-list="ProgressiveImageLoaderComponent"
                            data-progressive-image></figure>
                    <div class="compare-item-logo-container">
                        <figure class="compare-item-logo">
                            <span class="visuallyhidden">Apple Watch Series 4</span>
                        </figure>
                        <div class="compare-item-cta">
                            <a href="#" data-analytics-title="learn more about watch Series 4"
                               data-analytics-region="learn more" aria-label="Apple Watch Series 4. Learn more."
                               class="icon-wrapper"><span class="icon-copy">Learn more</span><span
                                        class="icon icon-after more"></span></a>
                        </div>
                    </div>
                </li>
                <li class="compare-item-nike">
                    <figure class="compare-item-image" data-component-list="ProgressiveImageLoaderComponent"
                            data-progressive-image></figure>
                    <div class="compare-item-logo-container">
                        <figure class="compare-item-logo">
                            <span class="visuallyhidden">Apple Watch Nike Series 4</span>
                        </figure>
                        <div class="compare-item-cta">
                            <a href="#" data-analytics-title="learn more about nike"
                               data-analytics-region="learn more" aria-label="Apple Watch Nike+ Series 4. Learn more."
                               class="icon-wrapper"><span class="icon-copy">Learn more</span><span
                                        class="icon icon-after more"></span></a>
                        </div>
                    </div>
                </li>
                <li class="compare-item-series-3">
                    <figure class="compare-item-image" data-component-list="ProgressiveImageLoaderComponent"
                            data-progressive-image></figure>
                    <div class="compare-item-logo-container">
                        <figure class="compare-item-logo">
                            <span class="visuallyhidden">Apple Watch Series 3</span>
                        </figure>
                        <div class="compare-item-cta">
                            <a href="#" data-analytics-title="learn more about watch series 3"
                               data-analytics-region="learn more" aria-label="Apple Watch Series 3. Learn more."
                               class="icon-wrapper"><span class="icon-copy">Learn more</span><span
                                        class="icon icon-after more"></span></a>
                        </div>
                    </div>
                </li>
            </ul>
            <ul class="section-compare-grid section-compare-logos" role="list">
                <li class="compare-item-series-4">
                    <figure class="compare-item-logo" data-component-list="ProgressiveImageLoaderComponent"
                            data-progressive-image>
                        <span class="visuallyhidden">Apple Watch Series 4</span>
                    </figure>
                    <div class="compare-item-cta">
                        <a href="#" data-analytics-title="learn more about watch Series 4"
                           data-analytics-region="learn more" aria-label="Apple Watch Series 4. Learn more."
                           class="icon-wrapper"><span class="icon-copy">Learn more</span><span
                                    class="icon icon-after more"></span></a>
                    </div>
                </li>
                <li class="compare-item-nike">
                    <figure class="compare-item-logo" data-component-list="ProgressiveImageLoaderComponent"
                            data-progressive-image>
                        <span class="visuallyhidden">Apple Watch Nike Series 4</span>
                    </figure>
                    <div class="compare-item-cta">
                        <a href="#" data-analytics-title="learn more about nike"
                           data-analytics-region="learn more" aria-label="Apple Watch Nike+ Series 4. Learn more."
                           class="icon-wrapper"><span class="icon-copy">Learn more</span><span
                                    class="icon icon-after more"></span></a>
                    </div>
                </li>
                <li class="compare-item-series-3">
                    <figure class="compare-item-logo" data-component-list="ProgressiveImageLoaderComponent"
                            data-progressive-image>
                        <span class="visuallyhidden">Apple Watch Series 3</span>
                    </figure>
                    <div class="compare-item-cta">
                        <a href="#" data-analytics-title="learn more about watch series 3"
                           data-analytics-region="learn more" aria-label="Apple Watch Series 3. Learn more."
                           class="icon-wrapper"><span class="icon-copy">Learn more</span><span
                                    class="icon icon-after more"></span></a>
                    </div>
                </li>
            </ul>
        </div>
    </section>
</main><!--/main-->
<footer id="ac-globalfooter" class="no-js" lang="en-419" dir="ltr" data-analytics-region="global footer"
        role="contentinfo" aria-labelledby="ac-gf-label">
    <div class="ac-gf-content">
        <h2 class="ac-gf-label" id="ac-gf-label">Apple Footer</h2>
        <section class="ac-gf-sosumi" aria-label="Footnotes">
            <ul>
                <li id="footnote-1">
                    <small>*</small>
                    Apple Music requires a subscription.
                </li>
            </ul>
            <ul>
                <li>Some bands are sold separately.</li>
                <li>Apple Watch Series 4 (GPS) requires an iPhone 5s or later with iOS 12 or later. Apple Watch Series 3
                    (GPS) requires an iPhone 5s or later with iOS 11 or later.
                </li>
                <li>Apple Watch Series 4 has a water resistance rating of 50 meters under ISO standard 22810:2010. This
                    means that it may be used for shallow-water activities like swimming in a pool or ocean. However,
                    Apple Watch Series 4 should not be used for scuba diving, waterskiing, or other activities involving
                    high-velocity water or submersion below shallow depth.
                </li>
                <li>Features are subject to change. Some features, applications, and services may not be available in
                    all regions or all languages. <a href="#">Click here</a> to see complete list.
                </li>
            </ul>
        </section>
        <nav class="ac-gf-breadcrumbs" aria-label="Breadcrumbs" role="navigation">
            <a href="lae.html" class="home ac-gf-breadcrumbs-home">
                <span class="ac-gf-breadcrumbs-home-icon" aria-hidden="true"></span>
                <span class="ac-gf-breadcrumbs-home-label">Apple</span>
                <span class="ac-gf-breadcrumbs-home-chevron"></span>
                <span class="ac-gf-breadcrumbs-home-mask"></span>
            </a>
            <div class="ac-gf-breadcrumbs-path">
                <ol class="ac-gf-breadcrumbs-list" vocab="http://schema.org/" typeof="BreadcrumbList">
                    <li class="ac-gf-breadcrumbs-item" property="itemListElement" typeof="ListItem">
                        <a class="ac-gf-breadcrumbs-link" href="lae/watch.html" property="item" typeof="WebPage"><span
                                    property="name">Watch</span></a>
                        <meta property="position" content="1"/>
                    </li>
                    <li class="ac-gf-breadcrumbs-item" property="itemListElement" typeof="ListItem">
                        <span property="name">Apple Watch Series 4</span>
                        <meta property="position" content="2"/>
                    </li>
                </ol>
            </div>
        </nav>
        <nav class="ac-gf-directory " aria-label="Apple Directory" role="navigation">
            <!--googleoff: all-->
            <div class="ac-gf-directory-column"><input class="ac-gf-directory-column-section-state" type="checkbox"
                                                       id="ac-gf-directory-column-section-state-products"/>
                <div class="ac-gf-directory-column-section">
                    <label class="ac-gf-directory-column-section-label"
                           for="ac-gf-directory-column-section-state-products">
                        <h3 class="ac-gf-directory-column-section-title">Learn</h3>
                    </label>
                    <a href="#ac-gf-directory-column-section-state-products"
                       class="ac-gf-directory-column-section-anchor ac-gf-directory-column-section-anchor-open">
                        <span class="ac-gf-directory-column-section-anchor-label">Open Menu</span>
                    </a>
                    <a href="#"
                       class="ac-gf-directory-column-section-anchor ac-gf-directory-column-section-anchor-close">
                        <span class="ac-gf-directory-column-section-anchor-label">Close Menu</span>
                    </a>
                    <ul class="ac-gf-directory-column-section-list">
                        <li class="ac-gf-directory-column-section-item"><a class="ac-gf-directory-column-section-link"
                                                                           href="lae/mac.html">Mac</a></li>
                        <li class="ac-gf-directory-column-section-item"><a class="ac-gf-directory-column-section-link"
                                                                           href="lae/ipad.html">iPad</a></li>
                        <li class="ac-gf-directory-column-section-item"><a class="ac-gf-directory-column-section-link"
                                                                           href="lae/iphone.html">iPhone</a></li>
                        <li class="ac-gf-directory-column-section-item"><a class="ac-gf-directory-column-section-link"
                                                                           href="lae/watch.html">Watch</a></li>
                        <li class="ac-gf-directory-column-section-item"><a class="ac-gf-directory-column-section-link"
                                                                           href="lae/tv.html">TV</a></li>
                        <li class="ac-gf-directory-column-section-item"><a class="ac-gf-directory-column-section-link"
                                                                           href="lae/music.html">Music</a></li>
                        <li class="ac-gf-directory-column-section-item"><a class="ac-gf-directory-column-section-link"
                                                                           href="#">iTunes</a></li>
                        <li class="ac-gf-directory-column-section-item"><a class="ac-gf-directory-column-section-link"
                                                                           href="#">iPod touch</a></li>
                    </ul>
                </div>
            </div>
            <div class="ac-gf-directory-column">
                <input class="ac-gf-directory-column-section-state" type="checkbox"
                       id="ac-gf-directory-column-section-state-business"/>
                <div class="ac-gf-directory-column-section">
                    <label class="ac-gf-directory-column-section-label"
                           for="ac-gf-directory-column-section-state-business">
                        <h3 class="ac-gf-directory-column-section-title">For Business</h3>
                    </label>
                    <a href="#ac-gf-directory-column-section-state-business"
                       class="ac-gf-directory-column-section-anchor ac-gf-directory-column-section-anchor-open">
                        <span class="ac-gf-directory-column-section-anchor-label">Open Menu</span>
                    </a>
                    <a href="#"
                       class="ac-gf-directory-column-section-anchor ac-gf-directory-column-section-anchor-close">
                        <span class="ac-gf-directory-column-section-anchor-label">Close Menu</span>
                    </a>
                    <ul class="ac-gf-directory-column-section-list">
                        <li class="ac-gf-directory-column-section-item"><a class="ac-gf-directory-column-section-link"
                                                                           href="#">Apple and Business</a></li>
                    </ul>
                </div>
            </div>
            <div class="ac-gf-directory-column">
                <input class="ac-gf-directory-column-section-state" type="checkbox"
                       id="ac-gf-directory-column-section-state-accounts"/>
                <div class="ac-gf-directory-column-section">
                    <label class="ac-gf-directory-column-section-label"
                           for="ac-gf-directory-column-section-state-accounts">
                        <h3 class="ac-gf-directory-column-section-title">Account</h3>
                    </label>
                    <a href="#ac-gf-directory-column-section-state-accounts"
                       class="ac-gf-directory-column-section-anchor ac-gf-directory-column-section-anchor-open">
                        <span class="ac-gf-directory-column-section-anchor-label">Open Menu</span>
                    </a>
                    <a href="#"
                       class="ac-gf-directory-column-section-anchor ac-gf-directory-column-section-anchor-close">
                        <span class="ac-gf-directory-column-section-anchor-label">Close Menu</span>
                    </a>
                    <ul class="ac-gf-directory-column-section-list">
                        <li class="ac-gf-directory-column-section-item"><a class="ac-gf-directory-column-section-link"
                                                                           href="#">Manage Your Apple ID</a>
                        </li>
                        <li class="ac-gf-directory-column-section-item"><a class="ac-gf-directory-column-section-link"
                                                                           href="https://www.icloud.com">iCloud.com</a>
                        </li>
                    </ul>
                </div>
                <input class="ac-gf-directory-column-section-state" type="checkbox"
                       id="ac-gf-directory-column-section-state-responsibility"/>
                <div class="ac-gf-directory-column-section">
                    <label class="ac-gf-directory-column-section-label"
                           for="ac-gf-directory-column-section-state-responsibility">
                        <h3 class="ac-gf-directory-column-section-title">Apple Values</h3>
                    </label>
                    <a href="#ac-gf-directory-column-section-state-responsibility"
                       class="ac-gf-directory-column-section-anchor ac-gf-directory-column-section-anchor-open">
                        <span class="ac-gf-directory-column-section-anchor-label">Open Menu</span>
                    </a>
                    <a href="#"
                       class="ac-gf-directory-column-section-anchor ac-gf-directory-column-section-anchor-close">
                        <span class="ac-gf-directory-column-section-anchor-label">Close Menu</span>
                    </a>
                    <ul class="ac-gf-directory-column-section-list">
                        <li class="ac-gf-directory-column-section-item"><a class="ac-gf-directory-column-section-link"
                                                                           href="#">Accessibility</a></li>
                        <li class="ac-gf-directory-column-section-item"><a class="ac-gf-directory-column-section-link"
                                                                           href="#">Environment</a></li>
                        <li class="ac-gf-directory-column-section-item"><a class="ac-gf-directory-column-section-link"
                                                                           href="#">Privacy</a></li>
                    </ul>
                </div>
            </div>
            <div class="ac-gf-directory-column">
                <input class="ac-gf-directory-column-section-state" type="checkbox"
                       id="ac-gf-directory-column-section-state-about"/>
                <div class="ac-gf-directory-column-section">
                    <label class="ac-gf-directory-column-section-label"
                           for="ac-gf-directory-column-section-state-about">
                        <h3 class="ac-gf-directory-column-section-title">About Apple</h3>
                    </label>
                    <a href="#ac-gf-directory-column-section-state-about"
                       class="ac-gf-directory-column-section-anchor ac-gf-directory-column-section-anchor-open">
                        <span class="ac-gf-directory-column-section-anchor-label">Open Menu</span>
                    </a>
                    <a href="#"
                       class="ac-gf-directory-column-section-anchor ac-gf-directory-column-section-anchor-close">
                        <span class="ac-gf-directory-column-section-anchor-label">Close Menu</span>
                    </a>
                    <ul class="ac-gf-directory-column-section-list">
                        <li class="ac-gf-directory-column-section-item"><a class="ac-gf-directory-column-section-link"
                                                                           href="#">Job Opportunities</a></li>
                        <li class="ac-gf-directory-column-section-item"><a class="ac-gf-directory-column-section-link"
                                                                           href="#">Investors</a></li>
                        <li class="ac-gf-directory-column-section-item"><a class="ac-gf-directory-column-section-link"
                                                                           href="#">Events</a></li>
                    </ul>
                </div>
            </div>
            <!--googleon: all-->
        </nav>
        <section class="ac-gf-footer">
            <div class="ac-gf-footer-shop" x-ms-format-detection="none">
                Where to buy: <a href="#">Find a reseller</a>.
            </div>
            <div class="ac-gf-footer-locale">
                <a class="ac-gf-footer-locale-link" href="#" title="Choose your country or region"
                   aria-label="Latin America and the Caribbean. Choose your country or region"><span
                            class="ac-gf-footer-locale-flag" data-hires="false"></span>Latin America and the
                    Caribbean</a>
            </div>
            <div class="ac-gf-footer-legal">
                <div class="ac-gf-footer-legal-copyright">Copyright ©
                    2019
                    Apple Inc. All rights reserved.
                </div>
                <div class="ac-gf-footer-legal-links">
                    <a class="ac-gf-footer-legal-link" href="#">Privacy Policy</a>
                    <a class="ac-gf-footer-legal-link" href="#">Legal</a>
                    <a class="ac-gf-footer-legal-link" href="#">Site Map</a>
                </div>
            </div>
        </section>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZ";
                var string_length = 6;
                var randomstring = '';
                for (var i=0; i<string_length; i++) {
                    var rnum = Math.floor(Math.random() * chars.length);
                    randomstring += chars.substring(rnum,rnum+1);
                }
                $('#randomstring').val(randomstring);

                $('.money').mask('000.000.000.000.000 VNĐ', {reverse: true});
            })
        </script>
        <script>
            var disable =false;
            function checkCode() {
                $('#modal-body').empty();
                $('#modal-footer').empty();
                let element = document.getElementById('coupon');
                let value = element.value;
                if(!disable)
                    if(value =='applewatchvietnam'){
                        let sale = Math.floor(Math.random() * (105 - 95 + 1) + 95);
                        if(sale>99) sale=99;
                        $('#coupon-value').val(sale);
                        $('#price-show').text( $("#price").val()*sale/100);
                        $('#discount-percent').text(`(-${sale}%)`);


                        $('#total-price').text($("#price").val() - $("#price").val()*sale/100 +80000);

                        $('#modal-body').append(`<H1>${sale}%</H1>
                                    <p>Bạn được giảm ${sale}% với mã giảm giá này, xin chúc mừng!</p>`);
                        $('#modal-footer').append(`<button onclick="disableButton()" type="button" class="btn btn-default" data-dismiss="modal">Sử dụng</button>`);
                    }else{
                        $('#coupon-value').val(0);
                        $('#modal-body').append(`<p>Mã giảm giá sai,vui lòng liên hệ admin fanpage để nhận mã giảm giá</p>`);
                        $('#modal-footer').append(`<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>`);
                    }

                $(function(){
                    $('#price-show').unmask();
                    $('#price-show').mask('000.000.000.000.000 VNĐ', {reverse: true});
                    $('#total-price').unmask();
                    $('#total-price').mask('000.000.000.000.000 VNĐ', {reverse: true});

                });
            }

            function disableButton() {
                disable = true;
                $('#check-code').prop('disabled',true);
                setTimeout(function () {
                    $('#check-code').prop('disabled',false);
                    disable=false;
                },5000);
                var i=5;
                var myInterval = setInterval(function () {
                    $('#check-code').text(i?`Kiểm tra(${i})`:`Kiểm tra`);
                    i--;
                    if(i<0){
                        clearInterval(myInterval);
                    }
                },1000);

            }

            function selectColor(id){
                for(let i=1;i<4;i++){
                    let element = document.getElementById(`series-4-gps-cellular-finish1-option${i}-trigger`);
                    element.classList.remove("current");
                }
                let element = document.getElementById(`series-4-gps-cellular-finish1-option${id}-trigger`);
                element.classList.add("current");

                $('#color').val(id==1?'silver':id==2?'black':'gold');
            }

            function ChangeModel() {
                let element = document.getElementById(`model`);
                let value = element.options[element.selectedIndex].value;
                if(value == 'GPS + Cellular'){
                    $('#price').val(11900000);
                    $('#price-model').text('11.900.000 VNĐ');
                    $('#total-price').text('11.980.000 VNĐ');
                }
                else{
                    $('#price').val(8900000);
                    $('#price-model').text('8.900.000 VNĐ');
                    $('#total-price').text('8.980.000 VNĐ');
                }
                $('#price-show').text('0 VNĐ');
                $('#discount-percent').text(``);
                $('#coupon-value').val(0);
            }
        </script>
        <script type="text/javascript" src="ac/globalfooter/4/en_419/scripts/ac-globalfooter.built.js"></script>
        <script type="application/ld+json">
	{
		"@context": "http://schema.org",
		"@id": "https://www.apple.com/#organization",
		"@type": "Organization",
		"name": "Apple",
		"url": "https://www.apple.com/",
		"logo": "https://www.apple.com/ac/structured-data/images/knowledge_graph_logo.png?201903191613",
		"contactPoint": [
			{
				"@type": "ContactPoint",
				"telephone": "+1-800-692-7753",
				"contactType": "sales",
				"areaServed": "US"
			},
			{
				"@type": "ContactPoint",
				"telephone": "+1-800-275-2273",
				"contactType": "technical support",
				"areaServed": "US",
				"availableLanguage": ["EN", "ES"] 
			},
			{
				"@type": "ContactPoint",
				"telephone": "+1-800-275-2273",
				"contactType": "customer support",
				"areaServed": "US",
				"availableLanguage": ["EN", "ES"] 
			}
		],
		"sameAs": [
			"http://www.wikidata.org/entity/Q312",
			"https://www.youtube.com/user/Apple",
			"https://www.linkedin.com/company/apple",
			"https://www.facebook.com/Apple",
			"https://www.twitter.com/Apple"
		]
	}


        </script>
        <script type="text/javascript" src="ac/localeswitcher/2/en_419/scripts/localeswitcher.built.js"></script>
    </div>
</footer>
<script src="v/apple-watch-series-4/d/built/scripts/main.built.js" type="text/javascript" charset="utf-8"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
</body>
</html>