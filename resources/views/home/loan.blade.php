@extends('layouts.app')

@section('title', 'Main page')

@section('content')
    <div class="tech-loan">
        <div class="tech-loan__banner" style="background-image: url('{{ url('/images/need-to-loan-bg.png') }}')"></div>
        <div class="container">
            <div class="text-center mt-4">
                <div class="linear-title">
                    các gói sản phẩm vay
                </div>
            </div>
            <div class="tech-loan__product mt-8">
                <ul class="list-wapper">
                    <li class="list-item">
                        <div class="list-inner">
                            <img src="{{ url('/images/loan-img.png') }}"/>
                            <p class="mb-0 text-center mt-h1">Vay theo lương</p>
                        </div>
                    </li>
                    <li class="list-item">
                        <div class="list-inner">
                            <img src="{{ url('/images/loan-img.png') }}"/>
                            <p class="mb-0 text-center mt-h1">Vay theo lương</p>
                        </div>
                    </li>
                    <li class="list-item">
                        <div class="list-inner">
                            <img src="{{ url('/images/loan-img.png') }}"/>
                            <p class="mb-0 text-center mt-h1">Vay theo lương</p>
                        </div>
                    </li>
                    <li class="list-item">
                        <div class="list-inner">
                            <img src="{{ url('/images/loan-img.png') }}"/>
                            <p class="mb-0 text-center mt-h1">Vay theo lương</p>
                        </div>
                    </li>
                    <li class="list-item">
                        <div class="list-inner">
                            <img src="{{ url('/images/loan-img.png') }}"/>
                            <p class="mb-0 text-center mt-h1">Vay theo lương</p>
                        </div>
                    </li>
                    <li class="list-item">
                        <div class="list-inner">
                            <img src="{{ url('/images/loan-img.png') }}"/>
                            <p class="mb-0 text-center mt-h1">Vay theo lương</p>
                        </div>
                    </li>
                    <li class="list-item">
                        <div class="list-inner">
                            <img src="{{ url('/images/loan-img.png') }}"/>
                            <p class="mb-0 text-center mt-h1">Vay theo lương</p>
                        </div>
                    </li>
                    <li class="list-item">
                        <div class="list-inner">
                            <img src="{{ url('/images/loan-img.png') }}"/>
                            <p class="mb-0 text-center mt-h1">Vay theo lương</p>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="tech-loan__step mt-6">
                <div class="text-center">
                    <div class="linear-title">
                        vay ngay chỉ với 4 bước
                    </div>
                </div>
                <div class="tech-loan__step__inner">
                    <ul class="list-wapper">
                        <li class="list-item">
                            <div class="list-inner step">
                                <div>
                                    <p class="mb-0 step-number">Bước 1</p>
                                    <p class="mb-0 text-uppercase step-title">đăng ký vay</p>
                                </div>
                                <div class="thumb">
                                    <img src="{{ url('/images/need-to-loan-step1.png') }}"/>
                                </div>
                            </div>
                            <div class="process-bar"><span></span></div>
                            <div class="list-inner content">
                                <span>Hoàn tất thông tin ở form đăng ký, đội ngũ CashTech sẽ liên hệ tư vấn ngay lập tức</span>
                            </div>
                        </li>
                        <li class="list-item">
                            <div class="list-inner step">
                                <div>
                                    <p class="mb-0 step-number">Bước 2</p>
                                    <p class="mb-0 text-uppercase step-title">Chuẩn bị hồ sơ</p>
                                </div>
                                <div class="thumb">
                                    <img src="{{ url('/images/need-to-loan-step2.png') }}"/>
                                </div>
                            </div>
                            <div class="process-bar"><span></span></div>
                            <div class="list-inner content">
                                <span>Hoàn tất thông tin ở form đăng ký, đội ngũ CashTech sẽ liên hệ tư vấn ngay lập tức</span>
                            </div>
                        </li>
                        <li class="list-item">
                            <div class="list-inner step">
                                <div>
                                    <p class="mb-0 step-number">Bước 3</p>
                                    <p class="mb-0 text-uppercase step-title">Nhận xét duyệt</p>
                                </div>
                                <div class="thumb">
                                    <img src="{{ url('/images/need-to-loan-step1.png') }}"/>
                                </div>
                            </div>
                            <div class="process-bar"><span></span></div>
                            <div class="list-inner content">
                                <span>Hoàn tất thông tin ở form đăng ký, đội ngũ CashTech sẽ liên hệ tư vấn ngay lập tức</span>
                            </div>
                        </li>
                        <li class="list-item">
                            <div class="list-inner step">
                                <div>
                                    <p class="mb-0 step-number">Bước 4</p>
                                    <p class="mb-0 text-uppercase step-title">Nhận khoản vay</p>
                                </div>
                                <div class="thumb">
                                    <img src="{{ url('/images/need-to-loan-step4.png') }}"/>
                                </div>
                            </div>
                            <div class="process-bar"><span></span></div>
                            <div class="list-inner content">
                                <span>Hoàn tất thông tin ở form đăng ký, đội ngũ CashTech sẽ liên hệ tư vấn ngay lập tức</span>
                            </div>
                        </li>
                        <li class="list-item">
                            <div class="list-inner step"></div>
                            <div class="process-bar"><span></span></div>
                            <div class="list-inner content"></div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="tech-loan__choose">
            <div class="container">
                <div class="tech-loan__choose__inner">
                    <div class="left-side">
                        <img src="{{ url('/images/need-to-loan-choose.png') }}"/>
                    </div>
                    <div class="right-side">
                        <div class="title fw-bold text-uppercase">VÌ SAO NÊN CHỌN VAY TẠI CASHTECH ?</div>
                        <ul class="list-wapper">
                            <li class="list-item">
                                <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                Đăng ký thông tin online đơn giản
                            </li>
                            <li class="list-item">
                                <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                Xét duyệt nhanh chóng, giải ngân trong ngày
                            </li>
                            <li class="list-item">
                                <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                Không thế chấp tài sản
                            </li>
                            <li class="list-item">
                                <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                Không cần người bảo lãnh
                            </li>
                            <li class="list-item">
                                <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                Bảo mật khoản vay 100%
                            </li>
                            <li class="list-item">
                                <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                Liên kết với các Ngân hàng lớn trên toàn quốc
                            </li>
                            <li class="list-item">
                                <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                Uy tín hơn 8 năm hoạt động trong lĩnh vực tài chính
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection